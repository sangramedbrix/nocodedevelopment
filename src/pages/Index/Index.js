import { NavLink } from 'react-router-dom';

function Index() {
  return (
    <>
      <h1>Index Dev team</h1>
      <div style={{ display: 'flex', flexDirection: 'column', gap: '15px', margin: '40px' }}>
        <NavLink to={'/dashboard-public'}>Dashboard</NavLink>
        <NavLink to={'/creator-public'}>Old form creator</NavLink>
        {/* Put new links here  */}
      </div>
    </>
  );
}

export default Index;
