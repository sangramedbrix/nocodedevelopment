import React, { useState , useEffect} from "react";
import 'antd/dist/antd.css';
import TextHeadingH3 from "../../components/Text_headings/TextHeadingH3";
import { Button, Divider , Input } from 'antd';
import LoginImageBanner from "../../assets/images/login_bg.png";
import InputCheckbox from '../../components/Inputs_fields/InputCheckbox';
import Google from '../../assets/images/google.png';
import LogoSmall from '../../assets/images/logo.png';
import "../../components/theme.css";
import "./style.css";
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import axios from "axios";


function AppComponent()
{
    const [userData, setUserData] = useState({ username: "", password: "" });
    const [errorMessage, setErrorMessage] = useState({ value: "" });
    const [errorEmailMessage, setEmailErrorMessage] = useState({ value: "" });
    const [errorPasswordMessage, setPasswordErrorMessage] = useState({ value: "" });
    
    let uname = localStorage.getItem("isAuthenticated");
	function isValid()
	{
		if(uname)
		{		
			// window.location.pathname = "/landingpage";
		}
	}
	isValid();
	useEffect(() => 
	{
    
    }, [isValid]);

    const handleInputChange = (e) => 
    {
        setUserData((prevState) => {
        return {
            ...prevState,
            [e.target.name]: e.target.value,
        };
        });
    };
  
   

  const handleSubmit = (e) => 
  {
    e.preventDefault();
    //if username or password field is empty, return error message
    if (userData.username === "") 
	{
        if (userData.password!= "") 
        {
            setPasswordErrorMessage((prevState) => ({
                value: "",
            }));      
        }
        setEmailErrorMessage((prevState) => ({
            value: "This field is required",
        }));   
    } 
    if (userData.password === "") 
	{
        if (userData.username!= "") 
        {
            setEmailErrorMessage((prevState) => ({
                value: "",
            }));      
        }
        setPasswordErrorMessage((prevState) => ({
            value: "This field is required",
        }));      
    } 
    else if (userData.username=== "" && userData.password=== "") 
	{
        setEmailErrorMessage((prevState) => ({
            value: "This field is required",
        })); 
        setPasswordErrorMessage((prevState) => ({
            value: "This field is required",
        }));
    } 
	else if (userData.username!= "" && userData.password!= "") 
	{
        setEmailErrorMessage((prevState) => ({
            value: "",
        })); 
        setPasswordErrorMessage((prevState) => ({
            value: "",
        }));
        
        axios.post('https://hexanocode.herokuapp.com/login',{email:userData.username, password:userData.password})
        .then((response) => 
        {          
            localStorage.setItem('isAuthenticated',JSON.stringify(response.data.data)); 
            window.location.pathname = "/landingpage";             
        })
        .catch(e =>  setErrorMessage((prevState) => ({ value: "Invalid username/password" })))
    } 
	else 
	{
        
    }
  };

    return (
        <>
            <div className='flex-container'>
                <div className='containers'>
                    <div style={{
                        display: 'flex',
                        alignSelf: 'flex-start',
                        margin: '9% 0 0 6%'
                    }}>
                         <img alt="logo_small" src={LogoSmall} /> 
                    </div>
                    <div className='login-form-box'>
                        <TextHeadingH3 text_h3="Login" className='login-title' />
                        <div className='loginsubtext'>Provide your credentials to proceed , please.</div>
                        <div className='text-signin-box mtb-3'>
                            <div className='signin-text'><img src={Google} /> Sign in with Google</div>
                        </div>
                        <div className='mtb-5'>
                            <Divider plain >Or Sign in with Email</Divider>
                        </div>
                        <div className=''>
                            <div>
                            {errorMessage.value && (
                                <p className="text-danger"> {errorMessage.value} </p>
                            )}
                            </div>
                            <div className='label mtb-2'>Email</div>                            
                            <Input size="medium" className='text-input-box'
                                placeholder="email@website.com"
                                name="username"
                                onChange={(e) => handleInputChange(e)}                               
                            />
                            <span>
                                {errorEmailMessage.value && (
                                    <p className="text-danger"> {errorEmailMessage.value} </p>
                                )}
                            </span>
                        </div>
                        <div className=''>
                            <div className='label mtb-2'>Password</div>
                            <Input.Password size="medium" className='text-input-box'
                                placeholder="*******"
                                name="password"
                                onChange={(e) => handleInputChange(e)}
                                iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                            />
                            <span>
                                {errorPasswordMessage.value && (
                                    <p className="text-danger"> {errorPasswordMessage.value} </p>
                                )}
                            </span>
                        </div>
                        <div className='mtb-2' style={{ justifyContent: "space-between", display: "flex" }}>
                            <div className="login_checkbox" ><InputCheckbox check_text='Remember Me' className='remember-me' /></div>
                            <div ><Button type="link" block className='forgot-password'>Forgot Password?</Button></div>
                        </div>
                        <div className='mtb-1 mt-2' >
                            {/* <ButtonLogin block  button_title='Login' /> */}
                            <Button className='login-button-text login-button' onClick={handleSubmit} className='login-button' block>Login</Button>
                        </div>
                        <div>
                       
                        </div>
                    </div>
                </div>

                <div className='imgcontainer'> 
                <div >
                        <div className="side-img m-auto">
                    <img alt="example" width= '90%' height= "90%" src={LoginImageBanner} />
                    </div>
                    <div className='banner-text'>
                        <div className='banner-title'>Design. Build. Launch</div>
                        <div className='banner-subtitle'>Effortlessly Create Your Website, Dashboards, Forms. <br></br>No Code Required!!!</div>
                    </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default AppComponent; 