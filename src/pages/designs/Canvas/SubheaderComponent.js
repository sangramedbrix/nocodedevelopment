import { CheckCircleOutlined, CaretDownOutlined, EyeOutlined, RocketOutlined, MoreOutlined, RollbackOutlined } from '@ant-design/icons';
import React from 'react';
import 'antd/dist/antd.css';
import { PageHeader, Button, Input, Row, Col, Select } from 'antd';
import comment from '../../../assets/images/comment-icon.svg';
import '../../../components/theme.css'
import "./style.css";
const { Search } = Input;

const { Option } = Select;


const IconLink = ({ src, text }) => (
  <a className="example-link">
    <img className="example-link-icon" src={src} alt={text} />
    {text}
  </a>
);
const   SubheaderComponent = () => {
  return (
    <>
      {/* <div className='subheader-box'>
        <Row>
          <Col xl={12} lg={24} md={24} sm={24} xs={24}>
            <Row justify='start' align="middle">
              <Col xl={2} lg={2} md={2} sm={4} xs={2} className='cancel-box text-center' ><RollbackOutlined /> Cancel</Col>
              <Col xl={6} lg={6} md={6} sm={6} xs={7}>
                <div className='dropdown-border-none vertical-line'>
                  <Select placeholder="Module:" style={{ width: '100%' }}>
                    <Option value="jack">Jack</Option>
                    <Option value="lucy">Lucy</Option>
                    <Option value="tom">Tom</Option>
                  </Select>
                </div>
              </Col>
              <Col xl={6} lg={6} md={6} sm={6} xs={7}>
                <div className='dropdown-border-none vertical-line'>
                  <Select placeholder="Page:" style={{ width: '100%' }}>
                    <Option value="jack">Jack</Option>
                    <Option value="lucy">Lucy</Option>
                    <Option value="tom">Tom</Option>
                  </Select>
                </div>
              </Col>
              <Col xl={6} lg={6} md={6} sm={6} xs={8}> Status: <span className='status-div'>Action Pending </span></Col>
            </Row>

          </Col>


          <Col xl={12} lg={24} md={24} sm={24} xs={24}>
            <Row justify='end' align="middle" gutter={[10,0]}>
              <Col xl={6} lg={6} md={6} sm={7} xs={7}><IconLink src="https://gw.alipayobjects.com/zos/rmsportal/MjEImQtenlyueSmVEfUD.svg" text="Comments (0)" /></Col>
              <Col xl={4} lg={4} md={4} sm={4} xs={5}>
                <div className='dropdown-border'>
                <Button icon={<CheckCircleOutlined />} >Save {<CaretDownOutlined />}</Button>
                  <Select icon={<CheckCircleOutlined />} defaultValue="Save " style={{ width: '100%' }}>
                    <Option value="jack">Jack</Option>
                    <Option value="lucy">Lucy</Option>
                    <Option value="tom">Tom</Option>
                  </Select>
                </div>
              </Col>
              <Col className='preview-btn' xl={4} lg={4} md={4} sm={4} xs={5}><Button  icon={<EyeOutlined />} >Preview</Button></Col>
              <Col xl={4} lg={4} md={4} sm={4} xs={4}><Button type="primary" icon={<RocketOutlined />}> Publish </Button></Col>
              <Col xl={2} lg={2} md={2} sm={4} xs={1}><Button type="" icon={<MoreOutlined />}>  </Button></Col>
            </Row>

          </Col>
        </Row>
      </div> */}

      <div className='subheader_components'>
        <Row className='subheader_components_box' gutter={[0, 10]} align='middle'>
          <Col xl={12} lg={24} md={24} sm={24} xs={24}>
            <Row  className='first_row'  align='middle' gutter={[10,8]}>
              <Col xl={2} lg={2} md={4} sm={2} xs={5} className='cancel-box text-center' ><RollbackOutlined /> Cancel</Col>
              <Col className='dropdown-border-none vertical-line' xl={5} lg={6} md={5} sm={6} xs={12}>
                {/* <div > */}
                <Select placeholder="Module: Students" style={{ width: '100%' }}>
                  <Option value="jack">Students</Option>
                  <Option value="lucy">Students</Option>
                  <Option value="tom">Students</Option>
                </Select>
                {/* </div> */}
              </Col>
              <Col className='dropdown-border-none vertical-line' xl={7} lg={6} md={6} sm={5} xs={10}>
                {/* <div > */}
                <Select placeholder="Page: Personal Information" style={{ width: '100%' }}>
                  <Option value="jack">Personal Information</Option>
                  <Option value="lucy">Personal Information</Option>
                  <Option value="tom">Personal Information</Option>
                </Select>
                {/* </div> */}
              </Col>
              <Col xl={6} lg={6} md={6} sm={8} xs={12}> Status : <span className='status-div'> Action Pending </span></Col>
            </Row>
          </Col>
          <Col xl={12} lg={24} md={24} sm={24} xs={24}>
            <Row  className='seconed_row' align="middle" gutter={[10,8]}>
              <Col xl={6} lg={6} md={6} sm={7} xs={8}><IconLink src={comment} text=" Comments (0)" /></Col>
              <Col xl={4} lg={4} md={4} sm={4} xs={8}>
                <div className='preview-btn'>
                <Button style={{ width: '100%' }} icon={<CheckCircleOutlined />} > Save {<CaretDownOutlined />}</Button>
                  {/* <Select icon={<CheckCircleOutlined />} defaultValue="Save " style={{ width: '100%' }}>
                    <Option value="jack">Jack</Option>
                    <Option value="lucy">Lucy</Option>
                    <Option value="tom">Tom</Option>
                  </Select> */}
                </div>
              </Col>
              <Col className='preview-btn' xl={4} lg={4} md={4} sm={4} xs={8}><Button style={{ width: '100%' }}  icon={<EyeOutlined />} >Preview</Button></Col>
              <Col xl={4} lg={4} md={4} sm={4} xs={8}><Button style={{ width: '100%' }} type="primary" icon={<RocketOutlined />}> Publish </Button></Col>
              <Col  className='more-btn' xl={2} lg={2} md={2} sm={4} xs={3}><Button type="" icon={<MoreOutlined />}>  </Button></Col>

            </Row>
          </Col>
        </Row>
      </div>
    </>
  )
}

export default SubheaderComponent; 