import React from 'react';
import Headers from '../../../components/Header'
import AppComponent from './component';

const CanvasPage = () => {
  return (
    <>
    <Headers />
      <AppComponent />
    </>
  )
}

export default CanvasPage; 