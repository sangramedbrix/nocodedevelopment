import React from 'react';
import 'antd/dist/antd.css';
import SubheaderComponent from './SubheaderComponent';
import { Row, Col } from 'antd';
import PageTypeBox from '../../../components/Cards/PageTypeBox';
import Business1 from "../../../assets/images/Business decisions-bro 1.png";
import Pagetype1 from "../../../assets/images/page_types/Blank_Page.png";
import Pagetype2 from "../../../assets/images/page_types/Blog_Page.png";
import Pagetype3 from "../../../assets/images/page_types/Form.png";
import Pagetype4 from "../../../assets/images/page_types/Content.png";
import Pagetype5 from "../../../assets/images/page_types/Analytics.png";
import Pagetype6 from "../../../assets/images/page_types/Diagram.png";
import Pagetype7 from "../../../assets/images/page_types/Spatial.png";
import "./style.css";

const AppComponent = () => {
    return (
        <>
            <SubheaderComponent />
            <div className='container-box'>
                <div className='img-box' style={{ display: 'flex', justifyContent: 'center' }}    >
                    <img alt="example" src={Business1} />
                </div>
                <h1 style={{ textAlign: "center", margin: "50px 10px" , opacity: '0.6' }}>What kind of page you want to create ? </h1>
                <div >
                    <Row className="card-sm" justify='center'  gutter={[15,15]}>
                        <Col xl={3} lg={6} md={4} sm={4} xs={24}>
                            <PageTypeBox page_img_src={Pagetype1} page_title='Blank Page' />
                        </Col>
                        <Col xl={3} lg={6} md={4} sm={4} xs={24}>
                        <PageTypeBox page_img_src={Pagetype2} page_title='Blog Page' />
                        </Col>
                        <Col xl={3} lg={6} md={4} sm={4} xs={24}>
                            <PageTypeBox page_img_src={Pagetype3} page_title='Form' />
                        </Col>
                        <Col xl={3} lg={6} md={4} sm={4} xs={24}>
                            <PageTypeBox page_img_src={Pagetype4} page_title='Content' />
                        </Col>
                        <Col xl={3} lg={6} md={4} sm={4} xs={24}>
                            <PageTypeBox page_img_src={Pagetype5} page_title='Analyticsic' />
                        </Col>
                        <Col xl={3} lg={6} md={4} sm={4} xs={24}>
                            <PageTypeBox page_img_src={Pagetype6} page_title='Diagram' />
                        </Col>
                        <Col xl={3} lg={6} md={4} sm={4} xs={24}>
                            <PageTypeBox page_img_src={Pagetype7} page_title='Spatial' />
                        </Col>
                    </Row>
                </div>
            </div>
        </>
    )
}

export default AppComponent; 