import { CheckCircleOutlined, CaretDownOutlined, EyeOutlined, RocketOutlined, MoreOutlined, RollbackOutlined } from '@ant-design/icons';
import React from 'react';
import 'antd/dist/antd.css';
import { PageHeader, Button, Input, Row, Col, Select } from 'antd';
import "./style.css";
const { Search } = Input;

const { Option } = Select;


const IconLink = ({ src, text }) => (
  <a className="example-link">
    <img className="example-link-icon" src={src} alt={text} />
    {text}
  </a>
);
const SubheaderComponent = () => {
  return (
    <>
      <div className='subheader-box' style={{ padding: '5px 0 0 0' }}>
        <Row>
          <Col xl={12} lg={12} md={24} sm={24} xs={24}>
            <Row justify='start' align="middle">
              <Col xl={2} lg={2} md={2} sm={4} xs={2} className='cancel-box text-center' ><RollbackOutlined />Cancel</Col>
              <Col xl={6} lg={6} md={6} sm={6} xs={7}>
                <div className='dropdown-border-none vertical-line'>
                  <Select placeholder="Module:" style={{ width: '100%' }}>
                    <Option value="jack">Jack</Option>
                    <Option value="lucy">Lucy</Option>
                    <Option value="tom">Tom</Option>
                  </Select>
                </div>
              </Col>
              <Col xl={6} lg={6} md={6} sm={6} xs={7}>
                <div className='dropdown-border-none vertical-line'>
                  <Select placeholder="Page:" style={{ width: '100%' }}>
                    <Option value="jack">Jack</Option>
                    <Option value="lucy">Lucy</Option>
                    <Option value="tom">Tom</Option>
                  </Select>
                </div>
              </Col>
              <Col xl={6} lg={6} md={6} sm={6} xs={8}> Status: <span className='status-div'>Action Pending </span></Col>
            </Row>

          </Col>
          <Col xl={12} lg={12} md={24} sm={24} xs={24}>
            <Row justify='end' align="middle" gutter={[15, 15]}>
              <Col xl={6} lg={6} md={6} sm={8} xs={7}><IconLink src="https://gw.alipayobjects.com/zos/rmsportal/MjEImQtenlyueSmVEfUD.svg" text="Comments (0)" /></Col>
              <Col xl={4} lg={4} md={4} sm={4} xs={5}>
                <div className='dropdown-border'>
                {/* <Button icon={<CheckCircleOutlined />} >Preview {<CaretDownOutlined />}</Button> */}
                  <Select icon={<CheckCircleOutlined />} defaultValue="Save " style={{ width: '100%' }}>
                    <Option value="jack">Jack</Option>
                    <Option value="lucy">Lucy</Option>
                    <Option value="tom">Tom</Option>
                  </Select>
                </div>
              </Col>
              <Col className='preview-btn' xl={4} lg={4} md={4} sm={4} xs={5}><Button  icon={<EyeOutlined />} >Preview</Button></Col>
              <Col xl={4} lg={4} md={4} sm={4} xs={5}><Button type="primary" icon={<RocketOutlined />}> Publish </Button></Col>
              <Col xl={2} lg={2} md={2} sm={4} xs={1}><Button type="" icon={<MoreOutlined />}>  </Button></Col>
            </Row>

          </Col>
        </Row>
      </div>
    </>
  )
}

export default SubheaderComponent; 