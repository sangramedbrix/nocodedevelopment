import React, { useState, useEffect } from 'react';
import { Row, Col } from 'antd';
import UploadBox from './UploadBox';
import CreateModulePopup from '../../../components/Popup/CreateModulePopup'
import AddNewModule from '../../../components/Cards/AddNewModule';
import Tile1 from '../../../components/Subheader/Tile1';
import Tile2 from '../../../components/Subheader/Tile2';
import Tile3 from '../../../components/Subheader/Tile3';
import Tile4 from '../../../components/Subheader/Tile4';
import EmptyBox from './EmptyBox';
import ButtonList from './ButtonList';
import ViewBoxes from '../../../components/ViewBoxes/Index';
import CreatePagePopup from '../../../components/Popup/CreatePagePopup'
import axios from 'axios'
import moment from 'moment'
import { Layout } from 'antd';
import SideMenuComponent from "../../../components/Sidemenu";
const { Sider, Content } = Layout;
import Headers from '../../../components/Header';
import './style.css';

const AppCreated = () => {

  const [modules, setModules] = useState([])

  const getModules = () => {
    axios.get('https://hexanocode.herokuapp.com/getModules')
      .then((response) => {
        console.log("response is", response)
        setModules(response.data.data)
      })
      .catch((error) => console.log(error))
  }

  useEffect(() => {
    getModules()
  }, [])

  return (
    <>
      <Headers />
      <Layout>
        <Layout>
          <Sider><SideMenuComponent /></Sider>
          <Content>

            <div className='app_created pad-3'>
              <div className='subheader-container ptb-1' >
                <Row>
                  <Col xl={7} lg={7} md={12} sm={12} xs={24} ><Tile1 /></Col>
                  <Col xl={6} lg={6} md={12} sm={12} xs={24} ><Tile2 /></Col>
                  <Col xl={6} lg={5} md={12} sm={12} xs={24}><Tile3 /></Col>
                  <Col xl={5} lg={6} md={12} sm={12} xs={24}  ><Tile4 /></Col>
                </Row>
              </div>

              <div className='ptb-2'>
                <Row>
                  <Col xl={20} lg={20} md={24} sm={24} xs={24}>
                    <ButtonList />
                  </Col>
                  <Col xl={4} lg={4} md={24} sm={24} xs={24} >
                    <ViewBoxes />
                  </Col>
                </Row>
              </div>

              <div className='ptb-1'>
                <div className=''>

                  {/* <AddNewModule moduleName="Module List" module_id="sfd" card_title="dfd" card_date="dsf" card_description="dsd" />   */}

                  {console.log("length", typeof (modules.length))}
                  {
                    (() => {
                      if (modules.length > 0) {
                        return (
                          <>
                            <div className='flex-container-grid'>
                              <div><CreateModulePopup moduleName="Add New Module" getModulesData={getModules} /></div>
                              <AddNewModule moduleName="Module List" module_id="sfd" card_title="dfd" card_date="dsf" card_description="dsd" />
                            </div>
                          </>

                        )
                      } else {
                        return (
                          <>
                            <Row>
                              <Col xl={6} >
                                <div>
                                  <CreateModulePopup moduleName="Add New Module" getModulesData={getModules} />
                                </div>
                              </Col>
                              <Col xl={18} ><EmptyBox /></Col>
                            </Row>
                          </>
                        )

                      }
                    })()
                  }

                </div>
              </div>
            </div>
          </Content>
        </Layout>
      </Layout>
    </>
  )
}

export default AppCreated; 