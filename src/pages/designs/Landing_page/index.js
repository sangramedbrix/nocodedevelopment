import React from "react";
import AppComponent from './component';
import { Layout } from 'antd';
import SideMenuComponent from "../../../components/Sidemenu";
import Headers from "../../../components/Header";
const { Sider, Content } = Layout;

export default function LandingPage() {
  return (
    <>
    {/* <AppComponent /> */}
      <Headers />
      <Layout>
        <Layout>
          <Sider><SideMenuComponent /></Sider>
          <Content><AppComponent /></Content>
        </Layout>
      </Layout>
    </>
  );
}