import React,{useState,useEffect} from "react";
import 'antd/dist/antd.css';
import { useHistory } from 'react-router-dom';
// ./../Components/Text_headings/TextHeadingH2
import TextHeadingH3 from "../../../components/Text_headings/TextHeadingH3";
import { Row, Col, Form, Input, Select, Checkbox, Button, Divider,Card,Modal  } from 'antd';
// import LoginImageBanner from "Assets/Images/LoginImageBanner.png";
import InputText from '../../../components/Inputs_fields/InputText';
import InputTextArea from '../../../components/Inputs_fields/InputTextArea';
import InputFile from '../../../components/Inputs_fields/InputFile';

import InputPassword from '../../../components/Inputs_fields/InputPassword';
import InputCheckbox from '../../../components/Inputs_fields/InputCheckbox';
import SelectDropdown from '../../../components/Dropdowns/SelectDropdown';
import Dropdowns from '../../../components/Dropdowns/Dropdowns';
import Buttons from '../../../components/Buttons/Buttons';
import AppImageBanner from "../../../assets/images/app_banner.png";
import "../../../components/theme.css";
import ButtonFull from '../../../components/Buttons/ButtonFull';
// import Buttons from '../../../Components/Buttons/Buttons'
import axios from 'axios';
import swal from 'sweetalert';
import { FormOutlined,SlidersOutlined } from '@ant-design/icons';
import '../CreateNewApp/create_new_app.css';
// theme select popup import
import CardImageTop from '../../../components/Cards/CardImageTop';
import SearchBox from '../../../components/Dropdowns/SearchBox';
import ViewBoxes from '../../../components/ViewBoxes/Index'
import '../Theme_Select/theme_select.css';
import TextHeadingH4 from "../../../components/Text_headings/TextHeadingH4";

// end theme select import

const { Meta } = Card;

const colStyles = {
    flexBasis: "20%",
    width: "20%",

    '@media (max-width: 500px)': {
         colStyles : {
            flexBasis: "100%",
            width: "100%",
         }
      },
};

export default function LandingPageCard(props) {

    const [isModalVisible, setIsModalVisible] = useState(false);
    const [isModalVisibleTheme, setIsModalVisibleTheme] = useState(false);
    const [appCategory,setAppCategory] = useState([])
    const [appData,setAppData] = useState([])
    const history = useHistory();
    const [createNewApp,setCreateNewApp] = useState({
        name:'',
        category : '',
        description : '',
        isActive : 0,
        file: [],
        nameValidation : '',
        categoryValidation : '',
        descriptionValidation : '',
        imageValidation : ''
    })

    const dropdownOption = [
        { option: 'Select 1' },
        { option: 'Select 2' },
        { option: 'Select 3' }
    ]

    // const navigate = useNavigate()


    const getAppCategory = () => {
        axios.get('https://hexanocode.herokuapp.com/getAppCategory')
        .then((response) => {
                console.log(response)
                setAppCategory(response.data.data)
        })
        .catch(error => console.log(error))

    }

    const createdApp = () => {
        axios.get('https://hexanocode.herokuapp.com/getCreatedApp')
        .then((response) => {
                console.log("response data",response)
                setAppData(response.data.data[0])
               
        })
        .catch(error => console.log(error))

    }


    const nameChange = (name) => {
        console.log("app name",name)
        if(name!= ''){
            setCreateNewApp(prevState=>({...prevState, name: name,nameValidation:false }))
        }else {
            setCreateNewApp(prevState=>({...prevState, name: name, nameValidation:true}))
            console.log("empty")
        }

    }

    const categoryChange = (category) => {
        setCreateNewApp(prevState=>({...prevState, category: category,categoryValidation: false  }))
    }

    const discriptionChange = (description) => {
        if(description!= ''){
            setCreateNewApp(prevState=>({...prevState,description: description,descriptionValidation:false }))
        }else {
            setCreateNewApp(prevState=>({...prevState, description: description, descriptionValidation:true}))
            console.log("empty")
        }

    }

    const changeStatus = (status) => {
        if(status === 1){
            setCreateNewApp(prevState=>({...prevState, isActive: status }))
        }else if(status === 0){
            setCreateNewApp(prevState=>({...prevState, isActive: status }))
        }
    }

    const selectFile = (file) => {
            console.log("file",file)
            setCreateNewApp(prevState=>({...prevState, file: file,imageValidation: false  }))
    }

    const onSubmit = () => {
            if(createNewApp.name != '' && createNewApp.description != '' && createNewApp.category != '' && createNewApp.file.length != 0 ){
                const formData = new FormData();
                formData.append("image",createNewApp['file'].file.originFileObj)
                formData.append("name",createNewApp.name)
                formData.append("description",createNewApp.description)
                formData.append("category",createNewApp.category)
                formData.append("isActive",createNewApp.isActive)
                axios.post('https://hexanocode.herokuapp.com/createNewApp',formData).then((response) => {
                    console.log(response)
                    createdApp()
                    if(response.data.message === "App created!"){
                        setCreateNewApp(prevState=>({...prevState, name:'',description:'',category:''}))
                        swal({
                            title: "Create New App",
                            text: "App created",
                            icon: "success",
                            button: "Ok",
                          });

                          setTimeout(() => {
                              setIsModalVisibleTheme(true)

                          },2000)
                    }
                })
                .catch(error => console.log(error))
            }else {
              if(createNewApp.name == ''){
                setCreateNewApp(prevState=>({...prevState, nameValidation:true}))
              }else if(createNewApp.category == ''){
                setCreateNewApp(prevState=>({...prevState, categoryValidation: true }))
              }else if(createNewApp.description == '' ){
                setCreateNewApp(prevState=>({...prevState,descriptionValidation:true}))
              }else if(createNewApp.file.length == 0){
                setCreateNewApp(prevState=>({...prevState, imageValidation: true }))
              }
            }
    }   



    useEffect(() => {
        getAppCategory()
    }, [])

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };



  const openThemeSelect = () => {
    setIsModalVisibleTheme(true)
  }

  const handleOkTheme = () => {
    setIsModalVisibleTheme(false);
  };

  const handleCancelTheme = () => 
  {
    setIsModalVisibleTheme(false);
   // navigate('/app/module/list',{state:appData})
    history.push('/app/module/list',{state:appData})
  };


    return (
        <>


{/* modal for create your own app */}
<div className="modal-container">
        <Modal width={"100%"} height={"100%"}  style={{ top: 0 }} header={false} footer={false} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>

        <div className='flex-container'>
                <div className='containers'>
                    <div className='createApp-form-box'>
                        <div className='box-title'>
                            <TextHeadingH3 className='headertext text-center' text_h3="Create your own app today" />
                            <div className='subheadertxt text-center'>No code App Maker to Build Your App</div>
                        </div>
                        <div className='mtb-2 bolder'>
                            <div className='createAppSubtxt'>App Name *</div>
                            <InputText placeholder_text="Enter App Name" value={createNewApp.name} nameChangeFunction={nameChange} />
                            {
                                createNewApp.nameValidation === true
                                    ? <span style={{ color: 'red' }}>App name is required</span>
                                    : ""
                            }
                        </div>
                        <div className=' bolder'>
                            <div className='createAppSubtxt'>Category *</div>
                            <Dropdowns option={appCategory} dropdown_title='Select' value={createNewApp.category} categoryChangeFunction={categoryChange} />
                            {
                                createNewApp.categoryValidation === true
                                    ? <span style={{ color: 'red' }}>Category is required</span>
                                    : ""
                            }
                            {/* <SelectDropdown option={appCategory} dropdown_title='Select' value={createNewApp.category} dropdwon_size='medium' categoryChangeFunction={categoryChange} style={{width:'100%' }}/>
                            {
                                createNewApp.categoryValidation === true 
                                ? <span style={{color:'red'}}>Category is required</span>
                                : ""
                            } */}
                        </div>
                        <div className='mtb-1 bolder'>
                            <div className='createAppSubtxt'>Description</div>
                            <InputTextArea placeholder_text="" discriptionChangeFunction={discriptionChange} value={createNewApp.description} />
                            {
                                createNewApp.descriptionValidation === true
                                    ? <span style={{ color: 'red' }}>Description is required</span>
                                    : ""
                            }
                        </div>
                        <div className='mtb-1 '>
                            <div className='createAppSubtxt bolder'>Status</div>
                            <div className='checkbox2'> <InputCheckbox statusCheck={changeStatus} /> Yes, I agree to show my App to public</div>
                        </div>
                        <div className='mtb-2 '>
                            <div className='createAppSubtxt'>App Icon *</div>
                            <div className='text-input-upload'>
                                <InputFile text="No choosen file" fileSelect={selectFile} />
                                {
                                    createNewApp.imageValidation === true
                                        ? <span style={{ color: 'red' }}>App icon is required</span>
                                        : ""
                                }
                            </div>
                        </div>

                        <div className='mtb-1 btn-div' >
                            <div className='app-link'>
                                <Buttons button_color='link' button_title='Cancel' />
                            </div>
                            <div className='app-button'>
                                <Button onClick={onSubmit}>Select Theme</Button>
                                {/* <Buttons button_title='Select Theme' button_size='medium'  onClick={onSubmit} className='app-button-text' /> */}
                            </div>
                        </div>
                    </div>
                </div>

                <div className='appimgcontainer'>
                    <div className='create-banner-text'>
                        <div className='banner-titletop'>App Builder to build apps</div>
                        <div className='banner-subtitletop'>in five minutes without Code </div>
                    </div>
                    <div>
                        <div className='side-img text-center'><img className='m-auto' width="70%" alt="example" src={AppImageBanner} /></div>
                        <div className='create-banner-text'>
                            <div className='banner-title text-center'>Design. Build. Launch</div>
                            <div className='banner-subtitle'>Effortlessly Create Your Website, Dashboards, Forms. <br></br>No Code Required!!!</div>
                        </div>
                    </div>
                </div>
            </div>   
        </Modal>
        </div>
{/* end  modal for create your own app */}



{/* modal popup for select theme */}
<Modal width={"100%"} height={"100%"}  style={{ top: 0 }} header={false} footer={false} visible={isModalVisibleTheme} onOk={handleOkTheme} onCancel={handleCancelTheme}>
            {/* <div className='containers '>
                <div className='select_theam_header'>
                    <Row gutter={70}>
                        <Col sm={{ span: 24 }} xl={{ span: 1 }} ><img src='Assets/Images/themeselecticon.png' /></Col>
                        <Col sm={{ span: 24 }} xl={{ span: 8 }} >
                            <p className='p-tag'>App</p>
                            <h2 className='p-tag'>{appData.name}</h2>
                        </Col>
                    </Row>
                    <Divider />
                </div>
                <div className='select_theam_search_box'>
                    <SearchBox />
                </div>
                <Row className='cardrow mtb-2 mb-5' justify='' gutter={[25, 75]} >
                    <Col xl={4} lg={6} md={8} sm={12} xs={24}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                    <Col xl={4} lg={6} md={8} sm={12} xs={24}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                    <Col xl={4} lg={6} md={8} sm={12} xs={24}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                    <Col xl={4} lg={6} md={8} sm={12} xs={24}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                    <Col xl={4} lg={6} md={8} sm={12} xs={24}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                    <Col xl={4} lg={6} md={8} sm={12} xs={24}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                    <Col xl={4} lg={6} md={8} sm={12} xs={24}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                    <Col xl={4} lg={6} md={8} sm={12} xs={24}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                    <Col xl={4} lg={6} md={8} sm={12} xs={24}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                </Row>
              
            </div> */}

            <div className='containers pad-3'>
                <div className='select_theam_header'>
                    <Row gutter={[70, 0]}>
                        <Col xl={1} lg={1} md={1} sm={1} xs={1} ><img src='Assets/Images/themeselecticon.png' /></Col>
                        <Col xl={22} lg={20} md={20} sm={18} xs={18} >
                            <p className='p-tag app'>App</p>
                            <h2 className='p-tag app_name'>{appData.name}</h2>
                        </Col>
                    </Row>
                    <Divider />
                </div>
                <div className='select_theam_search_box'>
                    <Row>
                        <Col xl={20} lg={20} md={24} sm={24} xs={24} >
                            <SearchBox />
                        </Col>
                        <Col xl={4} lg={4} md={24} sm={24} xs={24} >
                            <ViewBoxes />
                        </Col>
                    </Row>

                </div>
                <Row className='cardrow mtb-2 mb-5' type="flex" gutter={[16, 25]} >
                    <Row className='' type="flex" gutter={[16, 30]}>
                        <Col className='theam_select_card_col'> <CardImageTop appData={appData} action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                        <Col className='theam_select_card_col'> <CardImageTop appData={appData} action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                        <Col className='theam_select_card_col'> <CardImageTop appData={appData} action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                        <Col className='theam_select_card_col'> <CardImageTop appData={appData} action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                        <Col className='theam_select_card_col'> <CardImageTop appData={appData} action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                        <Col className='theam_select_card_col'> <CardImageTop appData={appData} action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                        <Col className='theam_select_card_col'> <CardImageTop appData={appData} action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                        
                    </Row>
                </Row>

            </div>

</Modal>

{/* end modal popup for select theme */}

 
   
            <Card
                title={props.title}
                hoverable
                className="card-box"
                cover={<img alt="example" style={{ width: '85%', height: "100%",margin:'10px auto' }} src={props.banner_img} />}
            >
                <div className="small-font mar-30 pr-2 pl-2">{props.desc}</div>
                <Button icon={<FormOutlined />} className="mbt-5 mlr create-app-button-text create-app-button"  onClick={showModal} > Create New </Button>
                <Button icon={<SlidersOutlined />} className="mbt-5 mlr create-app-button-text2 create-app-button2"> Select Template </Button>
            </Card>
        </>
    );
}