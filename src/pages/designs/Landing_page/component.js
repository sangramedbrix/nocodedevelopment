import React from 'react';
import 'antd/dist/antd.css';
import banner1 from '../../../assets/images/img1.png';
import banner2 from '../../../assets/images/img2.png';
import "./style.css";
import LandingPageCard from './LandingPageCard';
import Or from '../../../assets/images/or.png';

const AppComponent = () => {
  return (
    <>
      <div className='container-box landing-page-card'>
        <h1 className='landing-page-title'>What Do you want to create ? </h1>
        <div className='card-container'>
          <div>
            <LandingPageCard title="Apps" banner_img={banner1} desc="Create New App And Edit Existing App From The List" />
          </div>
          {/* <div class="wrapper">
            <div class="line"></div>
            <div class="wordwrapper">
              <div class="word">or</div>
            </div>
          </div> */}
          <div className='or'><img alt='or' src={Or} height='80%' width='80%' /></div>
          <div>
            <LandingPageCard title="Custom Blocks" banner_img={banner2} desc="Create New Custom Block And Use Anywhere In The App" />
          </div>
        </div>
      </div>
    </>
  )
}

export default AppComponent; 