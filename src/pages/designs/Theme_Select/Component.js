import React from 'react';
import CardImageTop from '../../../components/Cards/CardImageTop';
import 'antd/dist/antd.css';
import SearchBox from '../../../components/Dropdowns/SearchBox';
import ViewBoxes from '../../../components/ViewBoxes/Index'
import './theme_select.css';


import { Row, Col, Divider } from 'antd';

const ThemeSelectComonent = (props) => {
    return (
        <>
            <div className='containers '>
                <div className='select_theam_header'>
                    <Row gutter={[70, 0]}>
                        <Col xl={1} lg={1} md={1} sm={1} xs={1} ><img src='Assets/Images/themeselecticon.png' /></Col>
                        <Col xl={22} lg={20} md={20} sm={18} xs={18} >
                            <p className='p-tag app'>App</p>
                            <h2 className='p-tag app_name'>Student Management</h2>
                        </Col>
                    </Row>
                    <Divider />
                </div>
                <div className='select_theam_search_box'>
                    <Row>
                        <Col xl={20} lg={20} md={24} sm={24} xs={24} >
                            <SearchBox />
                        </Col>
                        <Col xl={4} lg={4} md={24} sm={24} xs={24} >
                            <ViewBoxes />
                        </Col>
                    </Row>

                </div>
                <Row className='cardrow mtb-2 mb-5' type="flex" gutter={[16, 25]} >
                    <Row className='' type="flex" gutter={[16, 30]}>
                        <Col className='theam_select_card_col'> <CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                        <Col className='theam_select_card_col'> <CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                        <Col className='theam_select_card_col'> <CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                        <Col className='theam_select_card_col'> <CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                        <Col className='theam_select_card_col'> <CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                        <Col className='theam_select_card_col'> <CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                        <Col className='theam_select_card_col'> <CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' card_color='Color' /></Col>
                        
                    </Row>
                </Row>

            </div>

        </>
    )
}

export default ThemeSelectComonent;
