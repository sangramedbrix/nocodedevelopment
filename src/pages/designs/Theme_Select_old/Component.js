import React from 'react';
import CardImageTop from '../../../components/Cards/CardImageTop';
import InputText from '../../../components/Inputs_fields/InputText';
import SelectDropdown from '../../../components/Dropdowns/SelectDropdown';
import 'antd/dist/antd.css';
import "../../../components/theme.css";
import SearchBox from '../../../components/Dropdowns/SearchBox';
import './theme_select.css';

import { Row, Col, Divider } from 'antd';

const ThemeSelectComonent = (props) => {
    return (
        <>
            <div className='containers'>
                <div className='select_theam_header'>
                    <Row gutter={[0,0]}>
                        <Col sm={{ span: 24 }} xl={1} ><img src='Assets/Images/themeselecticon.png' /></Col>
                        <Col sm={{ span: 24 }} xl={5} >
                            <p className='p-tag'>App</p>
                            <h5 className='p-tag'>Student Management</h5>
                        </Col>
                    </Row>
                    <Divider />
                </div>
                <div className='select_theam_search_box'>
                <SearchBox />
                </div>
                <Row className='cardrow mtb-2' >
                    <Col sm={{ span: 2 }} xl={{ span: 4 }}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' /></Col>
                    <Col sm={{ span: 2 }} xl={{ span: 4 }}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' /></Col>
                    <Col sm={{ span: 2 }} xl={{ span: 4 }}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' /></Col>
                    <Col sm={{ span: 2 }} xl={{ span: 4 }}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' /></Col>
                    <Col sm={{ span: 2 }} xl={{ span: 4 }}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' /></Col>
                </Row>
                <Row className='cardrow mtb-2'>
                    <Col sm={{ span: 2 }} xl={{ span: 4 }}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' /></Col>
                    <Col sm={{ span: 2 }} xl={{ span: 4 }}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' /></Col>
                    <Col sm={{ span: 2 }} xl={{ span: 4 }}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' /></Col>
                    <Col sm={{ span: 2 }} xl={{ span: 4 }}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' /></Col>
                    <Col sm={{ span: 2 }} xl={{ span: 4 }}><CardImageTop action_footer='false' card_img_src='Assets/Images/selectTheam.png' card_title='Shades of Teal' card_description='Vibrant' /></Col>
                </Row>
            </div>

        </>
    )
}

export default ThemeSelectComonent;
