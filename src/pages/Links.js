import { Link } from 'react-router-dom';

function Links() {
  return (
    <div className="App">
      <h1>Index</h1>
      <nav
        style={{
          borderBottom: 'solid 1px',
          paddingBottom: '1rem'
        }}>
        <Link to="/form">Form Demo</Link> | <Link to="/creator">Creator</Link>
      </nav>
    </div>
  );
}

export default Links;
