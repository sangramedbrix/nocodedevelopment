import Dialog from '@reach/dialog';
import React, { useState } from 'react';
import { ReactFormGenerator } from 'react-form-builder2';

export default function Form() {
  const [formDataValue, setFormDataValue] = useState('');
  const [endpointValue, setEndpointValue] = useState('');
  const [showDialog, setShowDialog] = useState(false);

  const showPreview = () => {
    setShowDialog(true);
  };

  const closePreview = () => setShowDialog(false);

  const handleFormDataChange = (event) => {
    setFormDataValue(event.target.value);
  };

  const handleEndpointChange = (event) => {
    setEndpointValue(event.target.value);
  };

  const handleClick = () => {
    showPreview();
  };
  console.log('data');
  console.log(formDataValue);
  return (
    <div style={{ display: 'flex', flexDirection: 'column', margin: '20px' }}>
      <h2>Form</h2>
      <label>Enter form submit endpoint: </label>
      <input type="text" value={endpointValue} onChange={handleEndpointChange} />
      <label>Enter form data: </label>
      <textarea rows={20} value={formDataValue} onChange={handleFormDataChange} />
      <button
        className="btn btn-primary float-left"
        style={{ width: '200px', marginTop: '20px' }}
        onClick={handleClick}>
        Generate Form
      </button>

      {showDialog && (
        <Dialog aria-labelledby="label" isOpen={showDialog} onDismiss={closePreview}>
          <ReactFormGenerator
            download_path=""
            back_action="/"
            back_name="Back"
            answer_data={{}}
            action_name="Save"
            form_action={endpointValue}
            form_method="POST"
            // variables={this.props.variables}
            data={JSON.parse(formDataValue)}
          />

          <div className="modal-footer">
            <button type="button" className="btn btn-default" onClick={closePreview}>
              Close
            </button>
          </div>
        </Dialog>
      )}
    </div>
  );
}
