/* eslint-disable react/prop-types */
import Dialog from '@reach/dialog';
import React, { useState } from 'react';
import { ReactFormBuilder, ElementStore, Registry } from 'react-form-builder2';

const TestLabel = () => <h2>New Hexa Component</h2>;

// eslint-disable-next-line react/display-name
const TestInput = React.forwardRef((props, ref) => {
  const { name, defaultValue, disabled } = props;
  return (
    <input
      ref={ref}
      label="My new Input"
      name={name}
      defaultValue={defaultValue}
      disabled={disabled}
    />
  );
});

Registry.register('MyInput', TestInput);
Registry.register('TestComponent', TestLabel);

const items = [
  {
    key: 'Header'
  },
  {
    key: 'TextInput'
  },
  {
    key: 'TextArea'
  },
  {
    key: 'RadioButtons'
  },
  {
    key: 'Checkboxes'
  },
  {
    key: 'Image'
  },
  {
    key: 'TwoColumnRow'
  },
  {
    key: 'ThreeColumnRow'
  },
  {
    key: 'FourColumnRow'
  },
  {
    key: 'TestComponent',
    element: 'CustomElement',
    component: TestLabel,
    type: 'custom',
    field_name: 'test_component',
    name: 'Hexa Label',
    icon: 'fa fa-cog',
    static: true,
    props: { test: 'test_comp' },
    label: 'Label Test'
  },
  {
    key: 'MyInput',
    element: 'CustomElement',
    component: TestInput,
    type: 'custom',
    forwardRef: true,
    field_name: 'my_input_',
    name: 'Test Input',
    icon: 'fa fa-cog',
    props: { test: 'test_input' },
    label: 'Hexa Input'
  }
];

export default function Creator() {
  const [formdata, setFormData] = useState(null);
  const [showDialog, setShowDialog] = useState(false);
  ElementStore.subscribe((state) => handleUpdate(state.data));

  const onPost = () => {
    setShowDialog(true);
  };

  const close = () => setShowDialog(false);

  const handleUpdate = (data) => {
    console.log('update');
    console.log(data);
    setFormData(data);
  };

  return (
    <>
      <ReactFormBuilder edit toolbarItems={items} onChange={handleUpdate} data={formdata} />
      <button
        className="btn btn-primary float-left"
        style={{ marginLeft: '10px' }}
        onClick={onPost}>
        View Form
      </button>
      {showDialog && (
        <Dialog aria-labelledby="label" isOpen={showDialog} onDismiss={close}>
          <div>
            {JSON.stringify(formdata)}
            <button style={{ display: 'block' }} onClick={close}>
              Close
            </button>
          </div>
        </Dialog>
      )}
    </>
  );
}
