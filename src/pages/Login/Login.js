/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import logo_small from '../../assets/images/logo_small.png';
import landing_art from '../../assets/images/landing/landing_art.png';
import TextField from '../../components/TextField';
import PrimaryButtton from '../../components/PrimaryButton';
import './Login.css';
import { NavLink } from 'react-router-dom';

async function loginUser({ username, password }) {
  const user = {
    username,
    password
  };
  return new Promise((resolve) => setTimeout(resolve, 2000)).then(() => user);
}

export default function Login({ setLoggedUser }) {
  const [username, setUserName] = useState();
  const [password, setPassword] = useState();

  const handleSubmit = async (e) => {
    e.preventDefault();
    const user = await loginUser({
      username: 'Mock Username',
      token: 'noeiufn9q4ufboqfiubuib'
    });
    setLoggedUser(user);
  };

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        height: '100%'
      }}>
      <div
        id="rowcontainer"
        style={{
          display: 'flex',
          flexDirection: 'row',
          flexWrap: 'wrap',
          width: '100%',
          height: '100%'
        }}>
        <div
          style={{
            flex: 1,
            flexGrow: 1,
            minWidth: '400px',
            flexDirection: 'column',
            backgroundColor: 'white',
            width: '50%',
            height: '100%'
          }}>
          <div
            style={{
              display: 'flex',
              alignSelf: 'flex-start',
              marginTop: '75px',
              marginLeft: '100px'
            }}>
            <img alt="logo_small" src={logo_small} />
            <NavLink style={{ marginLeft: '50px' }} to={'/index'}>
              View Pages
            </NavLink>
          </div>
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              alignSelf: 'center'
            }}>
            <span
              style={{
                display: 'flex',
                color: 'black',
                fontWeight: 'bold',
                fontSize: '2rem',
                marginTop: '100px',
                justifyContent: 'center'
              }}>
              Sign In
            </span>

            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center'
              }}>
              <form onSubmit={handleSubmit} style={{ display: 'flex', width: '50%' }}>
                <ul
                  style={{
                    listStyleType: 'none',
                    margin: 0,
                    padding: 0,
                    width: '100%'
                  }}>
                  <li style={{ marginTop: '50px' }}>
                    <TextField
                      style={{ width: '100%' }}
                      variant="standard"
                      label="Email"
                      autoComplete="username"
                      defaultValue="email@website.com"
                      onChange={(e) => setUserName(e.target.value)}
                    />
                  </li>
                  <li style={{ marginTop: '50px' }}>
                    <TextField
                      style={{ width: '100%' }}
                      variant="standard"
                      type="password"
                      label="Password"
                      autoComplete="current-password"
                      defaultValue="******"
                      onChange={(e) => setPassword(e.target.value)}
                    />
                  </li>
                  <li style={{ marginTop: '50px' }}>
                    <PrimaryButtton type="submit" style={{ width: '100%', marginBottom: '4rem' }}>
                      Sign In
                    </PrimaryButtton>
                  </li>
                </ul>
              </form>
            </div>
          </div>
        </div>
        <div
          style={{
            flex: 1,
            flexDirection: 'column',
            minWidth: '400px',
            paddingTop: '50px',
            width: '50%',
            backgroundColor: '#723DFB'
          }}>
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              margin: '50px',
              justifyContent: 'center'
            }}>
            <img
              alt="landing_art"
              src={landing_art}
              style={{ width: '100%', maxWidth: '750px', height: 'auto' }}
            />
            <span
              style={{
                color: 'white',
                fontSize: '1rem',
                fontWeight: 'bold',
                textAlign: 'center',
                paddingTop: '30px'
              }}>
              Design. Build. launch
              <br />
            </span>
            <span
              style={{
                color: 'white',
                textAlign: 'center',
                paddingBottom: '30px',
                paddingTop: '10px'
              }}>
              Effortlessly Create Your Website, Dashboards, Forms.
              <br />
              No Code Required!!!
            </span>
          </div>
        </div>
      </div>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          backgroundColor: '#723DFB',
          height: '50px',
          justifyContent: 'center'
        }}>
        <span style={{ color: 'white', textAlign: 'center', fontSize: '11px' }}>
          © 2021 - NoCode App
        </span>
      </div>
    </div>
  );
}

Login.propTypes = {
  setLoggedUser: PropTypes.func.isRequired
};
