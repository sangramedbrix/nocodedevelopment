import useLoggedUser from '../hooks/useLoggedUser';

function Dashboard() {
  const { loggedUser } = useLoggedUser();
  return (
    <>
      <h1>Hi, {loggedUser?.username}</h1>
      <h1>Dashboard</h1>
    </>
  );
}

export default Dashboard;
