/* eslint-disable react/prop-types */
import React from 'react';
import Dialog from '@reach/dialog';
import '@reach/dialog/styles.css';

export default function FormResult(props) {
  const [showDialog, setShowDialog] = React.useState(!!props.showDialog);

  const close = () => setShowDialog(false);

  return (
    <Dialog aria-labelledby="label" isOpen={showDialog} onDismiss={close}>
      <div
        style={{
          display: 'grid',
          justifyContent: 'center',
          padding: '8px 8px'
        }}>
        {props.data}
        <button style={{ display: 'block' }} onClick={close}>
          Close
        </button>
      </div>
    </Dialog>
  );
}
