import React from 'react';
import ButtonOutline from '../Buttons/ButtonOutline';
import ButtonRoundOutline from '../Buttons/ButtonRoundOutline';

const RecentAppsBox = (props) => {
    return (
        <>
            <div className='RecentAppsBox' >
                <div className='app_box'>
                    <div className='app_box_text'>
                        <h3> {props.card_title} </h3>
                        <p className='sub_title'> {props.card_description} </p>
                    </div>
                    <div className='img_div'>
                        <img src={props.card_img_src}  />
                    </div>


                    <div className='recent_apps_list'>
                        <div className='recent_apps_list_item'>
                            <h5 className='p-tag'>{props.recent_apps_list_item_heading} </h5>
                            <p className='p-tag'>{props.recent_apps_list_item_page}  </p>
                            <p className='p-tag'>{props.recent_apps_list_item_date}  </p>
                        </div>
                        <div className='recent_apps_list_item'>
                            <h5 className='p-tag'>{props.recent_apps_list_item_heading} </h5>
                            <p className='p-tag'>{props.recent_apps_list_item_page}  </p>
                            <p className='p-tag'>{props.recent_apps_list_item_date}  </p>
                        </div>
                       
                        <div className='button_div'>
                            <ButtonRoundOutline button_color='primary' button_size='medium' button_title='View All App' />
                        </div>
                    </div>
                </div>
            </div>

        </>
    )
}

export default RecentAppsBox;
