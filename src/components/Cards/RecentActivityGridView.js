import React from 'react'
import 'antd/dist/antd.css';
import './cards.css';
import { Card, Divider } from 'antd';
import { MoreOutlined } from '@ant-design/icons';


const { Meta } = Card;



const RecentActivityGridView = (props) => {
    return (
        <>
            <div className='RecentActivityGridView'> 

                <div hovreable className='card' >
                    <div className='card_body' >

                        <div className='card_img'><img  src={props.recent_activity_icon} alt='cardTop' /></div>
                        <div className='card_text'>
                            {/* &#10247; */}

                            <h3> <span className='green'>New! </span> {props.card_title}  </h3>
                            <div className='sub_title' >
                                <p >{props.card_description} </p>
                                <p > {props.no_of_page}</p>
                            </div>
                            <p className='comments' > {props.card_coments}</p>
                            <h4 >{props.card_text} </h4>
                        </div>
                        <Divider />
                        <div className='card_footer' >
                            <p className='p-tag' > {props.card_footer_date} </p>
                            <p className='p-tag approved'> {props.card_footer_status} </p>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default RecentActivityGridView;
