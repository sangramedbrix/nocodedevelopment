import React from 'react';
import 'antd/dist/antd.css';
import ButtonOutline from '../Buttons/ButtonOutline';
// import Buttons from '../Buttons/Button';
import SelectDropdown from '../Dropdowns/SelectDropdown'; 
import InputEmaill from '../Inputs_fields/InputEmaill'; 

 
import {
    Switch 
  } from 'antd';

  


const ElementSetting = () => {
    const dropdownOption = [
        {option:'optionA'},
        {option:'optionB'},
        {option:'optionC'}
    ]
    return (
        <>
        
            <div className='ElementSetting'>
                <div className='elementsettingbox'>
                    <div className='elementsettingheader'>
                        <h3>Element Settings</h3>
                        <div className=''>
                            <ButtonOutline button_color='primary' button_size='small' button_title='Cancle' />
                            {/* <Buttons button_color='primary' button_size='small' button_title='Apply' /> */}
                        </div>
                    </div>
                        <SelectDropdown option={dropdownOption}  dropdwon_size='medium' dropdown_title='type' dropdwon_size='small' block />
                        <SelectDropdown option={dropdownOption}  dropdwon_size='medium' dropdown_title='type' dropdwon_size='small' block />
                        <InputEmaill />
                        <Switch defaultChecked    />
                        <Switch defaultChecked    />
                        <Switch defaultChecked    />
                        <Switch defaultChecked    />
                </div>
            </div>
            
            
        </>
    )
}

export default ElementSetting;
