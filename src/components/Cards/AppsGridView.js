import React from 'react';
import { Avatar, Divider, List } from 'antd';
import { BlockOutlined, FileOutlined } from '@ant-design/icons';
import './cards.css';

const AppsGridView = (props) => {
    return (
        <>
            <div className='AppsGridView'>

                <div hovreable className='card' style={{ width: 300 }}>
                    <div className='card_body' >

                        <div className='card_img'><img src={props.app_grid_icon} alt='cardTop' /></div>
                        <div className='card_text'>
                            {/* &#10247; */}
                            <h3> {props.card_title}    </h3>
                            <div className='sub_title' >
                                <p> {props.app_grid_date}  </p>
                            </div>
                            <div className='stetus_box'>
                                <p className='p-tag'> {<Avatar src="https://joeschmoe.io/api/v1/random" />} {<Avatar src="https://joeschmoe.io/api/v1/random" />} {<Avatar src="https://joeschmoe.io/api/v1/random" />} </p>
                                <p className='inprogress p-tag'> {props.status}  </p>
                            </div>
                        </div>
                        <Divider />
                        <div className='card_footer'>
                            <div className='module '>
                                {<FileOutlined  className='d-flex text-center' />}
                                <p className='p-tag'> {props.footer_module} </p>
                            </div>
                            <div className='page'>
                                {<FileOutlined  className='d-flex text-center' />}
                                <p className='p-tag'> {props.footer_page} </p>
                            </div>
                            <div className='comment '>
                                {<BlockOutlined  className='d-flex text-center' />}
                                <p className='p-tag'> {props.footer_comments} </p>
                            </div>



                        </div>
                    </div>
                </div>

            </div>
        </>
    )
}

export default AppsGridView;
