import React from 'react'
import { useHistory } from 'react-router';
import 'antd/dist/antd.css';
import { Card, Button } from 'antd';

const { Meta } = Card;

const CardImageTop = (props) => {
    const history = useHistory()
  console.log("App Datasssss", props.appData)

    return (
        <>
            <Card
                cover={<img alt="example" src={props.card_img_src} />}
                // actions='false'
                // actions={[props.action_footer]}
                // hoverable
                 className='card_img_top'
            >
                <p className='card-title p-tag'>{props.card_title} </p>
                <p className='card-description '>{props.card_description}</p>
                <p className='card-color p-tag'>{props.card_color}</p>
                <div className='select_div'>
                    <div className='d-flex'>
                        <p className='card-color-round p-tag'></p>
                        <p className='card-color-round p-tag'></p>
                    </div>
                    <div className='btn_div'>
                        <Button size='small'>Preview</Button>
                        <Button type="primary" size='small' onClick={() => {history.push('/app/module/list',{state:props.appData})}}>Select</Button>
                    </div>
                </div>

                {/* <Meta title={props.card_title} description={props.card_description} /> */}

            </Card>
        </>
    )
}

export default CardImageTop
