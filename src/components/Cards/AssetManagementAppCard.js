import React from 'react'
import './cards.css';
import { Divider } from 'antd';

import CountBox from './CountBox';


const AssetManagementAppCard = (props) => {
    return (
        <>
            <div className='AssetManagementAppCard'>
                <div className='AssetManagementAppCardDiv'>
                    <div className='card-body'>
                        <h3>{props.card_title} </h3>
                        <p className='sub_title'>{props.card_date} </p>
                        <div className='counter_box'>
                            <CountBox no_of_count='154' count_box_title='No. of Usages' />
                            <CountBox no_of_count='154' count_box_title='No. of Usages' />
                        </div>
                        <div className='counter_box'>
                            <CountBox no_of_count='154' count_box_title='No. of Usages' />
                            <CountBox no_of_count='154' count_box_title='No. of Usages' />
                        </div>
                        <Divider />
                        <div className='card_footer'>
                            <p className='p-tag sub_title'>{props.footer_text}</p>
                            <p className='p-tag inprogress'>{props.footer_status} </p>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default AssetManagementAppCard;
