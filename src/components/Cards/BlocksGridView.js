import React from 'react';
import { Avatar, Divider, List } from 'antd';
import { BlockOutlined,LockOutlined } from '@ant-design/icons';
import './cards.css';

const BlocksGridView = (props) => {
    return (
        <>
            <div className='BlocksGridView'>


                <div hovreable className='card' style={{ width: 300 }}>
                    <div className='card_body' >

                        <div className='card_img'><img src={props.block_grid_icon} alt='cardTop' /></div>
                        <div className='card_text'>
                            {/* &#10247; */}
                            <h3> {props.card_title}    </h3>
                            <div className='sub_title' >
                                <p> {props.app_grid_date} </p>
                            </div>
                            <div className='stetus_box'>
                                <p className='p-tag'> {<Avatar src="https://joeschmoe.io/api/v1/random" />} </p>
                                <p className='closed p-tag'> {props.status} </p>
                            </div>
                        </div>
                        <Divider />
                        <div className='card_footer' >
                        <p className='p-tag'>{<BlockOutlined />} {props.footer_comments}</p>
                        <p className='p-tag'>{<LockOutlined />}  {props.footer_public_private}</p>
                        </div>
                    </div>
                </div>


                

            </div>

        </>
    )
}

export default BlocksGridView;
