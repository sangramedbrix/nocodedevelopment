import React from 'react';
import Cards from './Card';
import CardImageTop from './CardImageTop';
import CardImageMiddle from './CardImageMiddle';
import CardImageBottom from './CardImageBottom';
import RecentActivityGridView from './RecentActivityGridView';
import AppsGridView from './AppsGridView';
import BlocksGridView from './BlocksGridView';
import RecentAppsBox from './RecentAppsBox';
import CountBox from './CountBox';
import ElementSetting from './ElementSetting';
import AssetManagementAppCard from './AssetManagementAppCard';
import AddNewModule from './AddNewModule';
import PageTypeBox from './PageTypeBox';
import './cards.css';
import '../theme.css';

const CardBox = () => {
    return (
        <>
            <div className='containers'>
                <div className='box-card'>
                    {/* <Cards /> */}
                    {/* <CardImageTop action_footer='Footer' card_img_src='https://picsum.photos/200' card_title='Card Title' card_description='this is the description' /> */}
                    {/* <CardImageMiddle card_img_src='https://picsum.photos/200' card_title='Card Title' card_description='this is the description' /> */}
                    {/* <CardImageBottom card_img_src='https://picsum.photos/200' card_title='Card Title' card_description='this is the description' /> */}
                    {/* <RecentActivityGridView recent_activity_icon='Assets/Images/cardTop.png' card_title='- Student Management' card_description='App : Student Management' no_of_page=' No of Pages : 12' card_coments='Latest Comments' card_text='Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint... ' card_footer_date='24 December 2021' card_footer_status='&bull; Approved'  /> */}
                    {/* <AppsGridView app_grid_icon='Assets/Images/AppsGridView.png' card_title='Asset Management' app_grid_date='27th December,2021' status='&bull; In Progress' footer_module='3 Modules' footer_page='51 page' footer_comments='15 Comments' /> */}
                    {/* <BlocksGridView block_grid_icon='Assets/Images/AppsGridView.png' card_title='Asset Management' app_grid_date='27th December,2021' status='&bull; In Progress'  footer_='51 page' footer_comments='15 Comments' footer_public_private='Private' /> */}
                    {/* <RecentAppsBox card_title='My recent Apps' card_description='Create new App and edit Existing App from the list' card_img_src='Assets/Images/RecentAppBox.png' recent_apps_list_item_heading='Staff Management' recent_apps_list_item_page='21 Pages' recent_apps_list_item_date='Last Edited On: 21/11/2021, 10:15:29 AM'  /> */}
                    {/* <CountBox no_of_count='154' count_box_title='No. of Usages' /> */}
                    {/* <ElementSetting /> */}
                    <AssetManagementAppCard card_title='Student Information' card_date='7th December, 2021' footer_text=' Create New Page' footer_status='&bull; In Progress'  />
                    
                    {/* <PageTypeBox page_img_src='Assets/Images/pagetype.png' page_title='Spatial'   /> */}
                </div>
            </div>
        </>
    )
}

export default CardBox;
