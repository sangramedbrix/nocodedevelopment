import React from 'react'
import { Card, Avatar } from 'antd';
import { EditOutlined, EllipsisOutlined, SettingOutlined } from '@ant-design/icons';

const { Meta } = Card;

const CardImageMiddle = (props) => {
    return (
        <>
             <Card
                style={{ width: 300 }}
                title={props.card_title} 
                cover={<img alt="example" src={props.card_img_src} />}
                actions={[ <h5>Footer</h5> ]}
                hoverable
            >
                <Meta description={props.card_description} />
                
            </Card>
        </>
    )
}

export default CardImageMiddle
