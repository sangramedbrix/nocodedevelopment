import React from 'react';
import TextHeadingH5 from '../Text_headings/TextHeadingH5';
import './cards.css'

const PageTypeBox = (props) => {
    return (
        <>
            <div className='page_type_box'>
                <div className='page_type_img_box_'>
                    <img src={props.page_img_src} alt='map' />
                </div>
                {/* <TextHeadingH5 className='p-tag' text_h5='Spatial' /> */}
                <div className='page_type_img_box_'>
                    <p className='p-tag d-flex text-center'>{props.page_title} </p>
                </div>

            </div>
        </>
    )
}

export default PageTypeBox;
