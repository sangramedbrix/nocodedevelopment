import React from 'react'

import { Card, Avatar } from 'antd';
import { EditOutlined, EllipsisOutlined, SettingOutlined } from '@ant-design/icons';
import anyone from "../../assets/images/LoginImageBanner.png";
const { Meta } = Card;


const CardImageBottom = (props) => {
    return (
        <>
        
        <Card
    hoverable
    style={{ width: 240 }}
   
  >
    <Meta title="Europe Street beat" description="www.instagram.com" 
         imgsrc={anyone}
    />
  </Card>
        </>
    )
}

export default CardImageBottom;
