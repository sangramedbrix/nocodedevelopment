import React, { useEffect, useState } from 'react'
import './cards.css';
import { Divider, Row, Col } from 'antd';
import CountBox from './CountBox';
import { useLocation,useHistory } from "react-router-dom";
// import Button from '../Buttons/Buttons';
import { Modal, Button, Form,Dropdown,Menu } from 'antd';
import { MoreOutlined } from '@ant-design/icons';
import CreatePagePopup from '../Popup/CreatePagePopup'
import axios from 'axios'
import moment from 'moment'
import 'antd/dist/antd.css';
import InputText from '../Inputs_fields/InputText';
import InputTextArea from '../Inputs_fields/InputTextArea';
import TextHeadingH4 from '../Text_headings/TextHeadingH4';
// import CreatePagePopup from '../Popup/CreatePagePopup';


const pagesMenu = (
    <Menu>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com"> Edit </a>
      </Menu.Item>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com"> Delete </a>
      </Menu.Item>
    </Menu>
    );


    const ModuleMenu = (
        <Menu>
             <Menu.Item>
                <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com"> View Pages </a>
            </Menu.Item>
          <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com"> Edit </a>
          </Menu.Item>
          <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com"> Delete </a>
          </Menu.Item>
        </Menu>
        );


const AssetManagementAppCard = (props) => {
    const [modules, setModules] = useState([])
    const [pages, setPages] = useState([])
    const [moduleId,setModuleId] = useState()
    const [createPage,setCreatePage] = useState({
        name:'',
        description : '',
        nameValidation : '',
        descriptionValidation : '',
    })
    console.log("props",props)
    const [isModalVisible, setIsModalVisible] = useState(false);
    const { state } = useLocation();
    console.log("state\value",state.state);
    const history = useHistory()

    const getModules = () => {
        axios.post('https://hexanocode.herokuapp.com/getModules',{appId:state.state._id})
            .then((response) => {
                console.log("response idsss", response)
                setModules(response.data.data)
            })
            .catch((error) => console.log(error))
    }

    


    const getPages = () => {
        axios.post('https://hexanocode.herokuapp.com/gePages',{moduleId:"61de7af378842127c7f7acc2"})
            .then((response) => {
                console.log("response is", response)
                setPages(response.data.data)
            })
            .catch((error) => console.log(error))
    }

    const showModal = (module_id) => {
        setModuleId(module_id)
        setIsModalVisible(true);
    };

    const handleOk = () => {
        console.log("ok")
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        console.log("cancel")
        setIsModalVisible(false);
    };
    const nameChange = (name) => {
        if(name!= ''){
            setCreatePage(prevState=>({...prevState, name: name,nameValidation:false }))
        }else {
            setCreatePage(prevState=>({...prevState, name: name, nameValidation:true}))
            console.log("empty")
        }

    }


    const discriptionChange = (description) => {
        if(description!= ''){
            setCreatePage(prevState=>({...prevState,description: description,descriptionValidation:false }))
        }else {
            setCreatePage(prevState=>({...prevState, description: description, descriptionValidation:true}))
            console.log("empty")
        }

    }


    const createPageModule = () => {
        console.log(createPage)
        if(createPage.name != '' && createPage.description != ''){
            axios.post('https://hexanocode.herokuapp.com/createPage',{moduleId:moduleId,pageName:createPage.name,description:createPage.description}).then((response) => {
                console.log(response)

                if(response.data.message === "Page created!"){
                    setCreatePage(prevState=>({...prevState, name:'',description:''}))
                    swal({
                        title: "Create New Page",
                        text: "Page created",
                        icon: "success",
                        button: "Ok",
                      });
                      setTimeout(() => {
                            history.push('/page_template')
                      },2000)
                }

            })
            .catch((error) => {
                console.log(error)
            })
        }else {
            if(createPage.name == ''){
                setCreatePage(prevState=>({...prevState, nameValidation:true}))
            }else if(createPage.description == '' ){
                setCreatePage(prevState=>({...prevState,descriptionValidation:true}))
            }
        }

      
    }


    useEffect(() => {
        getModules()
        getPages()
        console.log("call")
    }, [isModalVisible])


    return (
        <>
            {/* create page popup use here  */}
            <div className='create_page'>
                <Modal className='create_page_popup' visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} closable={false} header={false} footer={false} >
                    <TextHeadingH4 className='heading d-flex text-center' text_h4='Create a Module' />
                    <Form layout="vertical" >
                        <Form.Item label="Page Name" name="Page Name" rules={[{required: true},]}>
                            <InputText placeholder_text='Enter Page Name' value={createPage.name} nameChangeFunction={nameChange} />
                            {
                                createPage.nameValidation === true 
                                ? <span style={{color:'red'}}>Page name is required</span>
                                : ""
                            }
                        </Form.Item>

                        <Form.Item label="Description" rules={[{required: true},]}>
                            <InputTextArea discriptionChangeFunction={discriptionChange} value={createPage.description} />
                            {
                                createPage.descriptionValidation === true 
                                ? <span style={{color:'red'}}>Description is required</span>
                                : ""
                            }
                        </Form.Item>

                        <Form.Item wrapperCol={{offset: 4,span: 16}}>
                            <Button type="link" onClick={handleCancel} > Cancel</Button>
                            <Button type="primary" Type="submit" onClick={createPageModule}> Submit </Button>
                        </Form.Item>
                    </Form>
                </Modal>
            </div>

            {/* created modules display */}




            {
             (() => {
                 if(props.moduleName === "Module List"){
                   return (
                    modules.map((createdModule, i) => {
                        return (
                            <>
    
                                <Row>
                                    <Col xl={24}>
                                        <div className='AssetManagementAppCard'>
                                            <div className='AssetManagementAppCardDiv'>
                                                <div className='card-body'>
                                                <Row>
                                                    <Col xs={20}>
                                                    <h3 className='title'>{createdModule.name} </h3>
                                                    </Col>
                                                    <Col xs={4} className='option-dots'>
                                                        <Dropdown overlay={ModuleMenu}>
                                                        <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                                                        <MoreOutlined />
                                                        </a>
                                                        </Dropdown>
                                                    </Col> 
                                                </Row>
                                                    {/* <div style={{display:'flex'}}>
                                                    <h3 className='title'>{createdModule.name} </h3>
                                                    <div className='option-dots'>
                                                            <Dropdown overlay={ModuleMenu}>
                                                            <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                                                            <MoreOutlined />
                                                            </a>
                                                            </Dropdown></div> 
                                                        </div> */}
                                                    <p className='sub_title'>{moment(createdModule.createdAt).format('Do dddd, YYYY')} </p>
                                                    <div style={{ padding: '6% 0' }}>
                                                        <div className='small_txt'>
                                                            {createdModule.description}
                                                        </div>
                                                        <div className='large-txt'>
                                                            Start now creating pages.
                                                        </div>
                                                    </div>
                                                    <div className='btn-create'>
                                                        <Button type="primary" size="medium" className='Buton_small' onClick={() => showModal(createdModule._id)}>Create New Page</Button>
                                                        {/* <Button className='btn-txt' button_color='primary' button_title='Create New Page' onClick={myFunction} button_size='medium' /> */}
                                                    </div>
                                                </div>
                                            </div>
    
                                        </div>
    
                                    </Col>
    
    
                                </Row>
    
    
                            </>
                        )
                    })
                   )
                 }else if(props.moduleName === "Page List"){
                     return (
                        pages.map((page, i) => {
                            return (
                                <>
        
                                    <Row>
                                        <Col xl={24}>
                                            <div className='AssetManagementAppCard'>
                                                <div className='AssetManagementAppCardDiv'>
                                                    <div className='card-body'>
                                                        <div style={{display:'flex'}}>
                                                        <h3 className='title'>{page.name} </h3>
                                                        <div className='option-dots'>
                                                            <Dropdown overlay={pagesMenu}>
                                                            <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                                                            <MoreOutlined />
                                                            </a>
                                                            </Dropdown></div> 
                                                        </div>
                                                        <p className='sub_title'>{moment(page.createdAt).format('Do dddd, YYYY')} </p>
                                                        <div style={{ padding: '6% 0' }}>
                                                            <div className='small_txt'>
                                                                {/* {page.description} */}
                                                            </div>
                                                            <div className='large-txt'>
                                                            {page.description}
                                                            {/* {
                                                                (() => {
                                                                    var str = page.description
                                                                    if(str.length > 102){
                                                                        return str = str.substring(0,102)
                                                                    }
                                                                })()
                                                            } */}
                                                            </div>
                                                        </div>
                                                      
                                                    </div>
                                                </div>
        
                                            </div>
        
                                        </Col>
        
        
                                    </Row>
        
        
                                </>
                            )
                        })
        
                     )
                 }
             })()
            }


        </>
    )
}

export default AssetManagementAppCard;