import React from 'react';
import './cards.css';

const CountBox = (props) => {
    return (
        <>
            <div className='count_box'>
                <div className='count_number'>
                    <h3>{props.no_of_count}</h3>
                </div>
                <div className='count_text'>
                    <h5>{props.count_box_title} </h5>
                </div>
            </div>
        </>
    )
}

export default CountBox;
