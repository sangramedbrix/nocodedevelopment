import React, { useState } from 'react';
import 'antd/dist/antd.css';
import { Modal, Button, Form, } from 'antd';
import { DeleteTwoTone, DeleteFilled, WarningOutlined } from '@ant-design/icons';
import TextHeadingH3 from '../Text_headings/TextHeadingH3';
import Buttons from '../Buttons/Buttons'
import './popup.css';



const DeletePopup = (props) => {

    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleSubmit = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    // 

    const onFinish = (values) => {
        console.log('Success:', values);
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <>

            <Button type="primary" onClick={showModal}>  Delete  </Button>
            <div className='create_module'>
                <Modal visible={isModalVisible}
                    className='delete_popup'
                    onCancel={handleCancel}
                    // closable={false}
                    header={false}
                    footer={false}
                >
                    <div className='confirm_box'>
                        <div className='icon'> 
                            <WarningOutlined className='d-flex text-center' />
                        </div>
                        <div className='heading'>
                            <TextHeadingH3 text_h3='Are you sure you want to cancel this page?' />
                        </div>
                        <div className='description'>
                            <p>{props.popup_description} </p>
                        </div>
                        <div className='buttons_div d-flex text-center' >
                            <Buttons  button_color='primary' button_size='large' button_title='Yes!' />
                            <Buttons  button_color='danger' button_size='large' button_title='No, Go Back!' />
                        </div>
                    </div>
                </Modal>
            </div>
        </>
    )
}

export default DeletePopup;
