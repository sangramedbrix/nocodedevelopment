import React, { useState } from 'react';
import 'antd/dist/antd.css';
import { Modal, Button, Form, Checkbox } from 'antd';
import InputText from '../Inputs_fields/InputText';
import InputTextArea from '../Inputs_fields/InputTextArea';
import TextHeadingH4 from '../Text_headings/TextHeadingH4';
import './popup.css';


const FilterPopup = () => {
    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };
    return (
        <>
            <div className='filter_popup'>
                <Button type="primary" onClick={showModal}> Filter </Button>
                <Modal className='filter_popup_box'

                    visible={isModalVisible}
                    onOk={handleOk}
                    onCancel={handleCancel}
                    closable={false}
                    header={false}
                    footer={false}
                >
                    <Form
                        layout="vertical"
                    >
                        <InputText placeholder_text='Search ...' />
                        <Form.Item name="Sort_a_to_z" valuePropName="checked"
                            wrapperCol={{ offset: 1, span: 20, }} >
                            <Checkbox>Sort by A-Z</Checkbox>
                        </Form.Item>
                        <Form.Item name="Sort_z_to_a" valuePropName="checked"
                            wrapperCol={{ offset: 1, span: 20, }} >
                            <Checkbox>Sort by Z-A</Checkbox>
                        </Form.Item>
                        <Form.Item name="value_item_one" valuePropName="checked"
                            wrapperCol={{ offset: 1, span: 20, }} >
                            <Checkbox>Value Item One</Checkbox>
                        </Form.Item>
                        <Form.Item name="value_item_two" valuePropName="checked"
                            wrapperCol={{ offset: 1, span: 20, }} >
                            <Checkbox>Value Item Two</Checkbox>
                        </Form.Item>
                        <Form.Item name="value_item_three" valuePropName="checked"
                            wrapperCol={{ offset: 1, span: 20, }} >
                            <Checkbox>Value Item Three</Checkbox>
                        </Form.Item>

                        <Form.Item
                        // wrapperCol={{
                        //     offset: 0,
                        //     span: 24,
                        // }}
                        >
                            <div classnames="footer_button_div">
                                <Button type="link"> Reset</Button>
                                <Button type="primary" Type="submit"> Submit </Button>
                            </div>
                        </Form.Item>


                    </Form>
                </Modal>
            </div>
        </>
    )
}

export default FilterPopup;
