import React from 'react';
import 'antd/dist/antd.css'; 
import CreateModulePopup from './CreateModulePopup';
import CreatePagePopup from './CreatePagePopup';
import FilterPopup from './FilterPopup';
import ActionPopup from './ActionPopup';
import SavePopup from './SavePopup';
import EditPopup from './EditPopup';
import ConfrimBox from './ConfrimBox';
import DeletePopup from './DeletePopup';

const Popup = () => {
   
    return (
        <> 
            {/* <CreateModulePopup /> */}
            <CreatePagePopup />
            {/* <CreatePagePopup />
            <FilterPopup />
            <ActionPopup />
            <SavePopup />
            <EditPopup />
            <ConfrimBox />
            <DeletePopup popup_description='If you cancel this page, you will lose unsaved data and also
                                not saved in your automatic generated version.' /> */}

        </>
    )
}

export default Popup;
