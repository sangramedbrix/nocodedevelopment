import React, { useState } from 'react';
import 'antd/dist/antd.css';
import { Modal, Button, Form, Checkbox } from 'antd';
import TextHeadingH4 from '../Text_headings/TextHeadingH4';
import InputText from '../Inputs_fields/InputText';
import InputTextArea from '../Inputs_fields/InputTextArea';
import './popup.css';
import axios from 'axios'
import { PlusOutlined, SlidersOutlined } from '@ant-design/icons';
import { useLocation } from 'react-router-dom';




const CreateModulePopup = (props) => {
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [createModule,setModule] = useState({
        name:'',
        description : '',
        isActive : 0,
        nameValidation : '',
        descriptionValidation : '',
        checked : true
    })

    const {state} = useLocation()
    console.log("state valuesss",state.state)


    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleSubmit = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => 
    { 
        setIsModalVisible(false);
    };

    //
    

    const onFinish = (values) => {
        console.log('Success:', values);
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    const nameChange = (name) => {
        console.log("Module name",name)
        if(name!= ''){
            setModule(prevState=>({...prevState, name: name,nameValidation:false }))
        }else {
            setModule(prevState=>({...prevState, name: name, nameValidation:true}))
            console.log("empty")
        }

    }

    const discriptionChange = (description) => {
        console.log("description",description)
        if(description!= ''){
            setModule(prevState=>({...prevState,description: description,descriptionValidation:false }))
        }else {
            setModule(prevState=>({...prevState, description: description, descriptionValidation:true}))
            console.log("empty")
        }

    }

    function onChange(e) {
        console.log(`checked = ${e.target.checked}`);
        if(e.target.checked === true){
            setModule(prevState=>({...prevState, isActive: 1 }))
        }else if(e.target.checked === false){
            setModule(prevState=>({...prevState, isActive: 0 }))
        }
      }


      const createNewModule = () => {
          if(createModule.name != '' && createModule.description != ''){
            const newModuleData = {
                appId:state.state._id,
                name : createModule.name,
                description : createModule.description,
                isActive : createModule.isActive
            }
            axios.post('https://hexanocode.herokuapp.com/createModule',newModuleData).then((response) => {
                console.log(response)
                if(response.data.message === "Module created !"){
                    setIsModalVisible(false);
                    props.getModulesData()
                    setModule(prevState=>({...prevState, name: '', description:''}))
                    swal({
                        title: "Create Module",
                        text: "Module created",
                        icon: "success",
                        button: "Ok",
                      });
                }
            })
            .catch(error => console.log(error))
        }else {
          if(createModule.name == ''){
            setModule(prevState=>({...prevState, nameValidation:true}))
          }else if(createModule.description == '' ){
            setModule(prevState=>({...prevState,descriptionValidation:true}))
          }
        }
      }


    return (
        <>

            {/* <Button type="primary" onClick={showModal}>  Create a Module  </Button> */}
            <div className='upload-container'>
                <div className='upload-box'>
                    <div className='plusicon'>
                    < PlusOutlined onClick={showModal} />
                    </div>
                    <div className='addModule' >
                   {props.moduleName}
                    </div>
                </div>
                </div>
                {console.log(props.moduleName)}

                {
                    (() => {
                        if(props.moduleName === "Add New Module"){
                           return (
                            <div className='create_module'>
                            <Modal visible={isModalVisible}
                                className='create_module_popup'
                                onCancel={handleCancel}
                                closable={false}
                                header={false}
                                footer={false}
            
                            >
                                <TextHeadingH4 className='heading d-flex text-center' text_h4='Create a Module' />
                                
            
                                <Form
                                     layout="vertical"
                                >
                                    <Form.Item  label="Module Name" name="Module Name"
                                        rules={[
                                            {
                                                required: true, 
                                            },
                                        ]}
                                    >
                                        <InputText placeholder_text='Enter Module Name' value={createModule.name} nameChangeFunction={nameChange} />
                                        {
                                            createModule.nameValidation === true 
                                            ? <span style={{color:'red'}}>Module name is required</span>
                                            : ""
                                        }
                                    </Form.Item>
            
                                    <Form.Item label="Description" rules={[
                                            {
                                                required: true, 
                                            },
                                        ]}
                                    >
                                        <InputTextArea discriptionChangeFunction={discriptionChange}  value={createModule.description} />
                                        {
                                            createModule.descriptionValidation === true 
                                            ? <span style={{color:'red'}}>Module description is required</span>
                                            : ""
                                        }
                                    </Form.Item>
            
                                    <Form.Item label="Status" name="Status" valuePropName="checked"
                                        wrapperCol={{
                                            offset: 1,
                                            span: 20,
                                        }}
                                    >
                                        <Checkbox onChange={onChange}>Yes, I agree to show my module to public</Checkbox>
                                    </Form.Item>
            
                                    <Form.Item
                                        wrapperCol={{
                                            offset: 4,
                                            span: 16,
                                        }}
                                    >
                                        <Button type="link" onClick={handleCancel} > Cancel</Button>
                                        <Button type="primary" Type="submit" onClick={createNewModule}> Submit </Button>
                                    </Form.Item>
                                </Form>
            
                            </Modal>
                        </div>
                           )
                        }else if(props.moduleName === "Create New Page"){
                            return (
                               
                                    <div className='create_page'>
                                    <Modal className='create_page_popup' visible={isModalVisible}
                                className='create_module_popup'
                                onCancel={handleCancel}
                                closable={false}
                                header={false}
                                footer={false} >
                                        <TextHeadingH4 className='heading d-flex text-center' text_h4='Create a Page' />
                                        <Form layout="vertical" >
                                            <Form.Item label="Page Name" name="Page Name" rules={[{required: true},]}>
                                                <InputText placeholder_text='Enter Page Name' />
                                                
                                            </Form.Item>

                                            <Form.Item label="Description" rules={[{required: true},]}>
                                                <InputTextArea />
                                               
                                            </Form.Item>

                                            <Form.Item wrapperCol={{offset: 4,span: 16}}>
                                                <Button type="link" onClick={handleCancel} > Cancel</Button>
                                                <Button type="primary" Type="submit" > Submit </Button>
                                            </Form.Item>
                                        </Form>
                                    </Modal>
                                </div>

      
                            )
                        }
                    })()
                }
            
        </>
    )
}

export default CreateModulePopup;
