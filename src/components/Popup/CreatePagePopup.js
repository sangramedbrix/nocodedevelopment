import React, { useState,useEffect } from 'react';
import 'antd/dist/antd.css';
import { Modal, Button, Form } from 'antd';
import InputText from '../Inputs_fields/InputText';
import InputTextArea from '../Inputs_fields/InputTextArea';
import TextHeadingH4 from '../Text_headings/TextHeadingH4';

const CreatePagePopup = (props) => {
    const [isModalVisible, setIsModalVisible] = useState(false);
    console.log("props values",props)

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        console.log("ok")
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        console.log("cancel")
        setIsModalVisible(false);
    };

    useEffect(() => {
        setIsModalVisible(props.isModalVisible)
        console.log("props values effect",props)
    }, [props])


    return (
        <>
            <div className='create_page'>
                <Button type="primary" onClick={showModal}> Create page </Button>
                <Modal className='create_page_popup'
                    visible={isModalVisible}
                    onOk={handleOk}
                    onCancel={handleCancel}
                    closable={false}
                    header={false}
                    footer={false}
                >
                    <TextHeadingH4 className='heading d-flex text-center' text_h4='Create a Module' />
                    <Form
                         layout="vertical"
                    >
                        <Form.Item  label="Page Name" name="Page Name"
                            rules={[
                                {
                                    required: true, 
                                },
                            ]}
                        >
                            <InputText placeholder_text='Enter Page Name' />
                        </Form.Item>

                        <Form.Item label="Description" rules={[
                                {
                                    required: true, 
                                },
                            ]}
                        >
                            <InputTextArea />
                        </Form.Item> 

                        <Form.Item
                            wrapperCol={{
                                offset: 4,
                                span: 16,
                            }}
                        >
                            <Button type="link" onClick={handleCancel} > Cancel</Button>
                            <Button type="primary" Type="submit"> Submit </Button>
                        </Form.Item>
                    </Form>
                </Modal>
            </div>

        </>
    )
}

export default CreatePagePopup;
