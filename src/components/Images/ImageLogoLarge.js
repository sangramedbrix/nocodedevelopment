import React from 'react';
import './images.css';


const ImageLogoLarge = (props) => {
    return (
        <>
            <img src={props.img_src} style={{width:'350px'} } />
        </>
    )
}

export default ImageLogoLarge;
