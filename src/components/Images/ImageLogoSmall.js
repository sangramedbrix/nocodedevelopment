import React from 'react'
import './images.css';

const ImageLogoSmall = (props) => {
    return (
        <>
            <img src={props.img_src} style={{width:'150px'}}/>
        </>
    )
}

export default ImageLogoSmall;
