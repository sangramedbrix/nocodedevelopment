import React from 'react'
import ImageLogoSmall from './ImageLogoSmall';
import ImageLogoMedium from './ImageLogoMedium';
import ImageLogoLarge from './ImageLogoLarge';
import ImageLoginBsnner50_50 from './ImageLoginBsnner50_50';

const Images = () => {
    return (
        <>
            <div className='containers'>
                <ImageLogoSmall img_src='Assets/Images/logo.png' />
                <ImageLogoMedium img_src='Assets/Images/logo.png' />
                <ImageLogoLarge img_src='Assets/Images/logo.png' />
                <ImageLoginBsnner50_50 img_src='Assets/Images/LoginImageBanner.png' />
            </div>
        </>
    )
}

export default Images;
