import React from 'react'
import './images.css';


const ImageLogoMedium = (props) => {
    return (
        <>
            <img src={props.img_src} style={{width:'250px'} } />
            
        </>
    )
}

export default ImageLogoMedium;
