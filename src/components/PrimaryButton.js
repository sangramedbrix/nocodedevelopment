/* eslint-disable react/prop-types */
import MuiButton from '@mui/material/Button';

export default function PrimaryButtton(props) {
  const { style, ...rest } = props;
  const buttonStyle = {
    backgroundColor: '#723DFB'
  };
  return <MuiButton variant="contained" style={{ ...buttonStyle, ...style }} {...rest} />;
}
