import React from 'react';
import 'antd/dist/antd.css';
import './dropdown.css';
import { Row, Col, Input, Select } from 'antd';

const { Option, OptGroup } = Select;

const SearchBox = () => {
    return (
        <>
           <div className='Search_box'>
                <Row gutter={[10, 10]}>
                    <Col  xl={8} lg={8} md={9} sm={9} xs={24} ><Input placeholder="Search ..." /></Col>
                    <Col xl={4} lg={4} md={5} sm={5} xs={24} >
                        <Select defaultValue="Sort by Layout" style={{ width: '100%' }} >
                            <OptGroup label="Sort by ">
                                <Option value="">Sort by A to Z</Option> 
                                <Option value="">Sort by Z to A</Option> 
                            </OptGroup> 
                        </Select>
                    </Col>
                    <Col xl={4} lg={4} md={5} sm={5} xs={24} >
                    <Select defaultValue="Filter by " style={{ width: '100%' }} >
                            <OptGroup label="Filter by ">
                                <Option value="">Filter by A to Z</Option> 
                                <Option value="">Filter by Z to A</Option> 
                            </OptGroup> 
                        </Select>
                    </Col>
                    <Col xl={4} lg={4} md={5} sm={5} xs={24} >
                    <Select defaultValue="Last Date" style={{ width: '100%' }} >
                            <OptGroup label="Date ">
                                <Option value="">05 Jan 2022</Option> 
                                <Option value="">04 jan 2022</Option> 
                            </OptGroup> 
                        </Select>
                    </Col>
                </Row>
            </div>
        </>
    )
}

export default SearchBox;
