import React from 'react'
import 'antd/dist/antd.css';
import { Select } from 'antd';
import './dropdown.css'

const { Option } = Select;

const SelectDropdown = (props) => { 
    console.log("props",props)

    const handleChange = (event) => {
        props.categoryChangeFunction(event)
            // console.log("dfsd",event)
    }

    return (
        <>
            <Select name="category" size={props.dropdwon_size} value={props.value} onChange={handleChange} >
                {
                    props.option.map((optionValue) => {
                        return (
                            <Option value={optionValue.name}>{optionValue.name}</Option>
                        )
                    })
                } 
            </Select>
            {/* <Select defaultValue="lucy" style={{ width: 120 }}>
                <Option value="jack">Jack</Option>
                <Option value="lucy">Lucy</Option>
                <Option value="disabled" disabled>
                    Disabled
                </Option>
                <Option value="Yiminghe">yiminghe</Option>
            </Select> */}
        </>
    )
}

export default SelectDropdown;