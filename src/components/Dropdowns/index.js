import React from 'react'
import SelectDropdown from './SelectDropdown';
import "../theme.css";

const Dropdown = () => {
    const dropdownOption = [
        {option:'optionAnkit'},
        {option:'optionB'},
        {option:'optionC'}
    ]

    
    return (
        <>
            <div className='containers'>
                <SelectDropdown   option={dropdownOption} dropdown_title='Select following item' dropdwon_size='medium' />
                </div>
        </>
    )
}

export default Dropdown;