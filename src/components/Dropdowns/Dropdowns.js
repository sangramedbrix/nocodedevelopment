import React from 'react'
import 'antd/dist/antd.css';
import { Select } from 'antd';
import './dropdown.css'

const { Option } = Select;

const Dropdowns = (props) => { 
    console.log("props values")

    const handleChange = (event) => {
        props.categoryChangeFunction(event)
            // console.log("dfsd",event)
    }


    return (
        <>
            {/* <Select defaultValue={props.dropdown_title}  size={props.dropdwon_size} >
                {
                    props.option.map((optionValue) => {
                        return (
                            <Option value={optionValue.option}>{optionValue.option}</Option>
                        )
                    })
                } 
            </Select> */}
            <Select className='dd-box' onChange={handleChange} >
                {
                    props.option.map((optionValue) => {
                        return (
                            <Option value={optionValue.name}>{optionValue.name}</Option>
                        )
                    })
                } 
                {/* <Option value="jack">Jacxxk</Option>
                <Option value="lucy">Lucy</Option>
                <Option value="Yiminghe">yiminghe</Option> */}
            </Select>
        </>
    )
}

export default Dropdowns;