import React from 'react';
//import SiderDemo from './Sidemenu';
import './style.css';
import { Menu, Button } from 'antd';
import Home from '../../assets/images/Home.png';
import Blocks from '../../assets/images/Blocks.png';
import Craete from '../../assets/images/Create.png';
import Files from '../../assets/images/Files.png';
import Logout from '../../assets/images/Logout.png';
import {useHistory} from 'react-router-dom'
import {
//   AppstoreOutlined,
//   MenuUnfoldOutlined,
//   MenuFoldOutlined,
//   PieChartOutlined,
//   DesktopOutlined,
//   ContainerOutlined,
//   MailOutlined,
  HomeOutlined, AppstoreFilled, LogoutOutlined,FormOutlined ,SwitcherFilled
} from '@ant-design/icons';

const { SubMenu } = Menu;
// toggleCollapsed = () => {
//     this.setState({
//       collapsed: !this.state.collapsed,
//     });
//   };

const SideMenuComponent = () => {
      const history = useHistory()
    return (
        <>
         {/* <SiderDemo /> */}
         <div className='sidemenu'>
        {/* <Button type="primary" onClick={this.toggleCollapsed} style={{ marginBottom: 16 }}>
          {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined)}
        </Button> */}
        <Menu
          defaultSelectedKeys={['1']}
          defaultOpenKeys={['sub1']}
          mode="inline"
        //   theme="dark"
        //   inlineCollapsed={this.state.collapsed}
        >
          <Menu.Item>
            <img src={Home} />
            {/* <i className='nc-home'></i> */}
          </Menu.Item>
          <Menu.Item key="2">
          <img src={Blocks} />
          </Menu.Item>
          <Menu.Item key="3">
            <img src={Craete} />
            </Menu.Item>
          <Menu.Item>
          <img src={Files} />
          </Menu.Item>
          <Menu.Item key="2" onClick={() => {
            localStorage.removeItem('isAuthenticated')
            history.push('/login')
          }}>
          <img src={Logout} />
          </Menu.Item>
          
          {/* <SubMenu key="sub1" icon={<MailOutlined />} title="Navigation One">
            <Menu.Item key="5">Option 5</Menu.Item>
            <Menu.Item key="6">Option 6</Menu.Item>
            <Menu.Item key="7">Option 7</Menu.Item>
            <Menu.Item key="8">Option 8</Menu.Item>
          </SubMenu>
          <SubMenu key="sub2" icon={<AppstoreOutlined />} title="Navigation Two">
            <Menu.Item key="9">Option 9</Menu.Item>
            <Menu.Item key="10">Option 10</Menu.Item>
            <SubMenu key="sub3" title="Submenu">
              <Menu.Item key="11">Option 11</Menu.Item>
              <Menu.Item key="12">Option 12</Menu.Item>
            </SubMenu>
          </SubMenu> */}
        </Menu>
      </div>
        </>
    )
}

export default SideMenuComponent;