import React from 'react';
import "../../components/theme.css";
import './style.css';
import { Row, Col } from 'antd';
import ButtonRound from '../../components/Buttons/ButtonRound';
import Template from '../../assets/images/template.png';

const Tile3 = () => {
  return (
    <>
      <div className='tile3-info tile3'>
        <p className='title3-title'>App Template</p>
        <Row gutter={[]}>
          <Col xl={5} lg={5} md={5} sm={5} xs={8}>
            <img src={Template}   />
          </Col>
          <Col xl={14} lg={12} md={12} sm={12} xs={8}>
            <div class="pc-user-info">
              <div class="tile3-subtitle">
                <p>Sidemenu - Standard </p>
              </div>
            </div>
          </Col>
          <Col xl={5} lg={5} md={5} sm={5} xs={8} className='btn-blue'>
            <ButtonRound button_title='Change' button_size='small' />
          </Col>
        </Row>
      </div>
    </>
  )
}

export default Tile3; 