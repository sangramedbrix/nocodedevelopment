import React from 'react';
import ButtonRound from '../../components/Buttons/ButtonRound';
import { Row, Col } from 'antd';

const Tile4 = () => {
  return (
    <>
      <Row className='tile4' style={{ marginTop: '14%', marginLeft: '30%' }}>
        <Col xs={4} lg={5} md={8} sm={12} xs={24} align="top" className='btn-blue'>
          <ButtonRound button_title='Preview App' button_size='small' />
        </Col>
      </Row>
    </>
  )
}

export default Tile4; 