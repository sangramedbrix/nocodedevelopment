import React from 'react'
import "../../components/theme.css";
import { Row, Col } from 'antd';
import Sidelogo from '../../assets/images/unsplash.png';
import './style.css';

const Tile1 = (props) => {

  return (
    <>
      {/* <div className='flex-container'>
        <div className='img'  >
          <img src={Sidelogo}  />
        </div>
        <div className='text-content'>
          <div class="title">Student Management</div>
          <div className='subtitle'>By Josep Carr - 3rd December , 2021 </div>
          <div style={{display:'flex',justifyContent:'space-around',fontWeight:500}}>
            <div className=''>
              <p>1 Module</p>
            </div>
            <div className=''>
              <p>0 Page</p>
            </div>
          </div>
        </div>
      </div> */}
      <Row className='vertical tile1' gutter={[5, 25]}>
        <Col xl={9} lg={7} md={7} sm={7} xs={7}>
          <img src={Sidelogo} />
        </Col>
        <Col xl={15} lg={17} md={17} sm={17} xs={17}>
          <div class="tile1-info">
            <div class="title1-title ">{props.appName}</div>
            <div className='title1-subtitle '>by Josep Carr - 3rd December,2021</div>
            <div class="pc-user-title">
              <Row>
                <Col xl={12} lg={12} sm={12} xs={12}><div className='module'>
                  <p className='p-tag'>1 Module</p>
                </div></Col>
                <Col xl={12} lg={12} sm={12} xs={12}>
                  <div className='module'>
                    <p className='p-tag'>0 Page</p>
                  </div>
                </Col>
              </Row>
            </div>
          </div>
        </Col>

      </Row>
      {/* <div class="subheader-container-tile3">
        <div class="pc-user">
          <div class="pc-user-image">
            <img src={Sidelogo} />
          </div>
          <div class="pc-user-info">
            <div class="title">Student Management</div>
            <h5>by Josep Carr - 3rd December, 2021</h5>
            <div class="pc-user-title">
              <div style={{ display: 'flex', justifyContent: 'space-around', fontWeight: 500 }}>
                <div className=''>
                  <p>1 Module</p>
                </div>
                <div className=''>
                  <p>0 Page</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> */}
    </>
  )
}

export default Tile1;