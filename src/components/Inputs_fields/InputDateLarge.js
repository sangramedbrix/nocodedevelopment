import React from 'react';
import { DatePicker } from 'antd';

function onChange(value) {
    console.log('changed', value);
  }

const InputDateLarge = (props) => {
    return (
        <>
            <DatePicker onChange={onChange} size='large' className='mr-2' />
        </>
    )
}

export default InputDateLarge;