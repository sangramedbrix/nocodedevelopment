import React from 'react';
import { DatePicker } from 'antd';

function onChange(value) {
    console.log('changed', value);
  }

const InputDateMedium = (props) => {
    return (
        <>
            <DatePicker onChange={onChange} size='middle' className='mr-2' />
        </>
    )
}

export default InputDateMedium;