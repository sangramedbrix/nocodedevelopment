import React from 'react';
import { Button, Upload,Input } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import InputText from './InputText';
import '../../pages/designs/CreateNewApp/create_new_app.css';

const InputFile = (props) => {

    const handleChange = (response) => {
            console.log("response",response)
            props.fileSelect(response)
    }

    return (
        <>
            <Upload name="logo" action="/upload.do" listType="picture"  onChange={handleChange}  >
                {/* <InputText type="text" placeholder='fjf'   >ff</InputText> */}
                <div className='choose-file-btn'>
                <Button >Choose file</Button>{props.text}</div>
                {/* <Button icon={<UploadOutlined />} size={props.input_size} className='mtb-1' >{props.text}</Button> */}
            </Upload>

            {/* <Upload name="logo" action="/upload.do" listType="picture" style={{width:'100%'}} onChange={handleChange}>
                <InputText type="file"   style={{width:'100%'}} >{props.text}</InputText>
                
            </Upload>  */}
        </>
    )
}

export default InputFile;