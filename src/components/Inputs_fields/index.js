import React from 'react'
import InputText from './InputText';
import InputEmail from './InputEmaill';
import InputTextArea from './InputTextArea';
import InputFile from './InputFile';
import InputPassword from './InputPassword';
import "../theme.css";
import InputNumbers from './InputNumber';

const InputFields = () => {
    return (
        <>
            <div className='containers'>
                <InputText placeholder_text="Enter Text..." input_size='medium' />
                <InputEmail placeholder_text="Enter your Small Email" input_size='medium'   />
                <InputTextArea placeholder_text="Enter Your Message..." input_size='medium'   />
                <InputFile input_size='medium' text='Click to upload' />

                <InputPassword placeholder_text="Enter Your Password..." input_size='medium'  />
                <InputNumbers placeholder_text="Enter Your Numner" input_size='medium' />
            </div>
        </>
    )
}

export default InputFields;