import React from 'react';
import { DatePicker } from 'antd';

function onChange(value) {
    console.log('changed', value);
  }

const InputDateSmall = (props) => {
    return (
        <>
            <DatePicker onChange={onChange} size='small' className='mr-2' />
        </>
    )
}

export default InputDateSmall;