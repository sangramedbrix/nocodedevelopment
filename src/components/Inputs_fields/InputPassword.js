import React from 'react';
import { Input } from 'antd';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';

const InputPassword = (props) => {
    return (
        <>
            <Input.Password size={props.input_size} className='mtb-1'
                placeholder="input password"
                iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
            />
        </>
    )
}

export default InputPassword;