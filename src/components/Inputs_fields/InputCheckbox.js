import React from 'react';
import { Checkbox } from 'antd';



const InputCheckbox = (props) => {

    function onChange(e) {
        console.log(`checked = ${e.target.checked}`);
        if(e.target.checked === true){
            props.statusCheck(1)
        }else if(e.target.checked === false){
            props.statusCheck(0)
        }
      }

    return (
        <>
            <Checkbox onChange={onChange} size='small' >{props.check_text}</Checkbox>
        </>
    )
}

export default InputCheckbox;