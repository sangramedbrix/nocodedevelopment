import React from 'react';
import { Input } from 'antd';
import { useForm } from "react-hook-form";

const InputText = (props) => {
    const { register, handleSubmit, watch, formState: { errors } } = useForm();
    const handleChange =(event) => {
        props.nameChangeFunction(event.target.value)
    }
    return (
        <>
            <Input placeholder={props.placeholder_text} size={props.input_size} value={props.value}  onChange={handleChange} className='input-box-dark' ></Input>
           
        </>
    )
}

export default InputText;