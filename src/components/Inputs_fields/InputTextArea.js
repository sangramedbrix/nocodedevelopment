import React from 'react';
import { Input } from 'antd';
const { TextArea } = Input;

const InputTextArea = (props) => {

    const handleChange = (event) => {
        props.discriptionChangeFunction(event.target.value)
            // console.log("dfsd",event)
    }


    return (
        <>
         <TextArea rows={3} placeholder={props.placeholder_text} value={props.value} onChange={handleChange} className='input-box-dark'/>
        </>
    )
}

export default InputTextArea;