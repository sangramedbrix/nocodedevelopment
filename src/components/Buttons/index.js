import React from 'react'

import Button from './Button';
import ButtonFull from './ButtonFull';

import ButtonRound from './ButtonRound';
import ButtonFullRound from './ButtonFullRound';

import ButtonOutline from './ButtonOutline';
import ButtonFullOutline from './ButtonFullOutline';

import ButtonRoundOutline from './ButtonRoundOutline';
import ButtonFullRoundOutline from './ButtonFullRoundOutline';

const Buttons = () => {
    return (
        <>
            <div className='containers'>
            <Button button_color='danger' button_title='Button Small' button_size='medium' />
            <ButtonFull button_color='danger' button_title='Button Full' />

            <ButtonRound button_color='danger' button_title='Button Small Round' button_size='medium' />
            <ButtonFullRound button_color='danger' button_title='Button Full Round' />

            <ButtonOutline button_color='danger' button_title='Button small outline' button_size='medium' />
            <ButtonFullOutline button_color='danger' button_title='Button Full outline' />

            <ButtonRoundOutline button_color='danger' button_title='Button small Round outline' button_size='medium' />
            <ButtonFullRoundOutline button_color='danger' button_title='Button Full Round outline' />
            </div>        
        </>
    )
}

export default Buttons;