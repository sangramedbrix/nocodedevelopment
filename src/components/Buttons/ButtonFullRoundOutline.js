import React from 'react'
import 'antd/dist/antd.css';
import './button.css';
import { Button } from 'antd';

const ButtonFullRoundOutline = (props) => {
    return (
        <>
            <Button type={props.button_color} shape="round" className='Button_full_round_outline' block ghost>{props.button_title}</Button>
        </>
    )
}

export default ButtonFullRoundOutline;
