import React from 'react'
import 'antd/dist/antd.css';
import './button.css';
import { Button } from 'antd';
import "../theme.css";

const ButtonFull = (props) => {
    return (
        <>
             <Button type={props.button_color} className='Button_full' block>{props.button_title}</Button>
            
        </>
    )
}

export default ButtonFull;
