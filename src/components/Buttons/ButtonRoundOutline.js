import React from 'react'
import 'antd/dist/antd.css';
import './button.css';
import { Button } from 'antd';

const ButtonRoundOutline = (props) => {
    return (
        <>
            <Button type={props.button_color} size={props.button_size} shape="round" className='Button_small_round_outline mar-2' ghost>{props.button_title}</Button>
        </>
    )
}

export default ButtonRoundOutline;
