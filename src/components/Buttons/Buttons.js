import React from 'react';
import './button.css'
import 'antd/dist/antd.css';
import { Button } from 'antd';


const Buttons = (props) => {
   console.log("button props",props)
    return (
        <>
            <Button type={props.button_color} size={props.button_size} className='Buton_small' onClick={props.submitApp}>{props.button_title}</Button>
        </>
    )
}
export default Buttons;
