import React from 'react'
import 'antd/dist/antd.css';
import './button.css'
import { Button } from 'antd';

const ButtonFullRound = (props) => {
    return (
        <>
            <Button type={props.button_color} shape="round" className='Button_full_round' block>{props.button_title}</Button>
        </>
    )
}

export default ButtonFullRound;
