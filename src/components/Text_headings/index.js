import React from 'react'
import TextHeadingH1 from './TextHeadingH1';
import TextHeadingH2 from './TextHeadingH2';
import TextHeadingH3 from './TextHeadingH3';
import TextHeadingH4 from './TextHeadingH4';
import TextHeadingH5 from './TextHeadingH5';

const TextHeadings = () => {
    return (
        <>
            <div className='containers'>
                <TextHeadingH1 textColor='success' text_h1=" Heading 1" />
                <TextHeadingH2 textColor='secondary' text_h2=" Heading 2" />
                <TextHeadingH3 textColor='primary' text_h3=" Heading 3" />
                <TextHeadingH4 textColor='warning' text_h4=" Heading 4" />
                <TextHeadingH5 textColor='info' text_h5=" Heading 5" />
            </div>
        </>
    )
}

export default TextHeadings;