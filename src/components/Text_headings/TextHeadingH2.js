import React from 'react';
import { Typography } from 'antd';
const { Title } = Typography;

const TextHeadingH2 = (props) => {
    return (
        <>
            <Title type={props.textColor} level={2}> {props.text_h2}</Title>
        </>
    )
}
export default TextHeadingH2;
