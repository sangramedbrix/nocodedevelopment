import React from 'react';
import { Typography } from 'antd';
const { Title } = Typography;

const TextHeadingH5 = (props) => {
    return (
        <>
            <Title type={props.textColor} level={5}> {props.text_h5}</Title>
        </>
    )
}
export default TextHeadingH5;
