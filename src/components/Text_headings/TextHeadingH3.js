import React from 'react';
import { Typography } from 'antd';
const { Title } = Typography;

const TextHeadingH3 = (props) => {
    return (
        <>
            <Title type={props.textColor} level={3}> {props.text_h3}</Title>
        </>
    )
}
export default TextHeadingH3;
