import React from 'react';
import { Typography } from 'antd';
const { Title } = Typography;

const TextHeadingH4 = (props) => {
    return (
        <>
            <Title type={props.textColor} level={4}> {props.text_h4} {props.text1} {props.desc1} {props.desc2}</Title>
        </>
    )
}
export default TextHeadingH4;
