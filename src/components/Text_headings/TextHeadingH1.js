import React from 'react';
import { Typography } from 'antd';
const { Title } = Typography;

const TextHeadingH1 = (props) => {
    return (
        <>
            <Title type={props.textColor} level={1}> {props.text_h1}</Title>
        </>
    )
}
export default TextHeadingH1;
