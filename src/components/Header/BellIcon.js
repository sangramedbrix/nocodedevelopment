import React from 'react';
import { BellOutlined } from '@ant-design/icons';
import './header.css';
import { Divider } from 'antd';

const BellIcon = () => {
    return (
        <div className='bell-container'>
            <div className='bell'>
                {/* <Divider type='vertical'className='vl'></Divider>
                <BellOutlined />
                <Divider type='vertical'className='vl'></Divider> */}
                 <div className='bell vl'>
           
           <BellOutlined />
          
           </div>
            </div>
        </div>
    )
}

export default BellIcon
