import React from 'react';
import 'antd/dist/antd.css';
import './header.css';
import Loggedin from '../../assets/images/userprofile.png'


const LoginDetails = () => 
{
    const handleLogout = () => {
        localStorage.clear();
        window.location.pathname = "/login";
      };

    return (
        <>
            <div className="user-profile">
                <div className='user-details'>
                    <div className="bio">Welcome</div>
                    <div className="username">
                        Joseph Carr
                    </div>
                </div>
                <img className="avatar" src={Loggedin} alt="Ash" />
            </div>
        </>
    )
}

export default LoginDetails;