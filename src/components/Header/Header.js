import React from 'react';
import 'antd/dist/antd.css';
import { Col, Layout, Menu, Row } from 'antd';
import Logo from '../../assets/images/logo.png';
import './header.css';
import LoginDetails from './LoginDetails';
import BellIcon from './BellIcon';
import '../../components/index.css'
const { Header, Content } = Layout;

const Navbar = () => {
    return (
        <>
            <div className='nav_div'>
               
                <div className="nav">
                    <div className="nav-header">
                        <div className="nav-title">
                            <img src={Logo} />
                        </div>
                    </div> 
                    <div className="nav-links">
                        <div><BellIcon /></div>
                        <div><LoginDetails /></div>                        
                    </div>
                </div>
                {/* <Row className="nav">
                    <Col xs>
                    <div className="nav-header">
                        <div className="nav-title">
                            <img src={Logo} />
                        </div>
                    </div> 
                    </Col>
                    <Col>
                    <div className="nav-links">
                        <div><BellIcon /></div>
                        <div><LoginDetails /></div>
                    </div>
                    </Col>
                </Row> */}

            </div>
        </>
    )
}

export default Navbar;