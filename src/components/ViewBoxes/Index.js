import React from 'react';
import { Button, Tooltip } from 'antd';
import { AppstoreOutlined, BarsOutlined } from '@ant-design/icons';
import './style.css';

const ViewBoxes = () => {
    return (
        <div style={{ textAlign: 'right' }} className='btn-blue grid-list-btn'>
            <Tooltip title="Grid View">
                <Button icon={<AppstoreOutlined />} />
            </Tooltip>
            <Tooltip title="List View">
                <Button icon={<BarsOutlined />} />
            </Tooltip>
        </div>
    )
}

export default ViewBoxes;