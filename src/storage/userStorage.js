export function getStoredUser() {
  return localStorage.getItem('login/user');
}

export function storeUser(user) {
  localStorage.setItem('login/user', JSON.stringify(user));
}
