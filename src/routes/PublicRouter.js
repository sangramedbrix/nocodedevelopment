import '../App.css';
import { BrowserRouter, Route } from 'react-router-dom';
import ProtectedRoute from '../components/ProtectedRoute';
import LandingPage from '../pages/designs/Landing_page';
import Signin from '../pages/Login_Page';
import CreateNewApp from '../pages/designs/CreateNewApp';
import ThemeSelect from '../pages/designs/Theme_Select'
import AppCreated from '../pages/designs/AppCreated';
import PageList from '../pages/designs/PageList';
import PageTemplate from '../pages/designs/Canvas';
// import PageTemplate from '../pages/Canvas';

function App() {
  return (
    <BrowserRouter>     

      {/* <Route exact path='/login' component={Signin} />      
      <Route exact path='/landingpage' component={LandingPage} />
      <Route exact path='/app/create' component={CreateNewApp} />
      <Route exact path='/appthemeselect' component={ThemeSelect} />
      <Route exact path='/app/module/list/page/list' component={PageList} />
      <Route exact path='/app/module/list' component={AppCreated} />
      <Route exact path='/' component={LandingPage} />
      <Route exact path='/page_template' component={PageTemplate} />  */}



      <Route exact path='/login' component={Signin} />      
      <ProtectedRoute exact path='/landingpage' component={LandingPage} />
      <ProtectedRoute exact path='/app/create' component={CreateNewApp} />
      <ProtectedRoute exact path='/themeselect' component={ThemeSelect} />
      <ProtectedRoute exact path='/app/module/list/page/list' component={PageList} />
      <ProtectedRoute exact path='/app/module/list' component={AppCreated} />
      <ProtectedRoute exact path='/' component={LandingPage} />
      <ProtectedRoute exact path='/page_template' component={PageTemplate} />
    </BrowserRouter>
  ); 
}

export default App;
