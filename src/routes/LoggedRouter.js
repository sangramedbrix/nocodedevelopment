import React from 'react';
import { BrowserRouter, Route, Redirect } from 'react-router-dom';
import FormDemo from '../pages/formdemo';
import Creator from '../pages/creator';
import FormResult from '../pages/formresult';
import Dashboard from '../pages/Dashboard';
import Index from '../pages/Index/Index';

export default function LoggedRouter() {
  return (
    <BrowserRouter>
        <Route exact path="/dashboard" component={Dashboard} />
        <Route path="/index" component={Index} />

        {/* Routes from PoC */}
        <Route exact path="/formresult" component={FormResult} />
        <Route exact path="/form" component={FormDemo} />
        <Route exact path="/creator" component={Creator} />

        <Route exact path="*" component={<Redirect to="/dashboard" />} />
      

     
    </BrowserRouter>
  );
}
