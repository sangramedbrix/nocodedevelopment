import React, { useEffect, useState } from 'react';
// import useLoggedUser from './hooks/useLoggedUser';
import Loading from './pages/Loading';
// import LoggedRouter from './routes/LoggedRouter';
import PublicRouter from './routes/PublicRouter';

function App() 
{
  const [loading, setLoading] = useState(true);
  // const { loggedUser } = useLoggedUser();

  useEffect(() => 
  {
    setTimeout(() => setLoading(false), 2000);
  }, []);

  if (loading) {
    return <Loading />;
  }

  // if (!loggedUser) {
    return <PublicRouter />;
  // }

  // return <LoggedRouter />;
}

export default App;
